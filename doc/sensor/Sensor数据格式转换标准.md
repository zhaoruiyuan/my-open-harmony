**Sensor数据格式转换标准**

 Sensor对外提供的接口函数数据结构参考[interface文件](https://gitee.com/openharmony/drivers_interface/tree/master/sensor/v1_0)，HDI接口对应的器件类型和数据单位如下表HDI列,驱动模型对外提供的DDI接口和结构体定义参考sensor/driver目录中的[include文件](https://gitee.com/openharmony/drivers_framework/tree/master/model/sensor/driver/include)，对应器件类型和数据单位如下表DDI列。

| 器件类型   | HDI type类型 | DDI                                         | 数据转换大小 | HDI                                        |
| ---------- | ----------- | ------------------------------------------- | ------------ | ------------------------------------------ |
| 加速度     | HDF_SENSOR_TYPE_ACCELEROMETER(1) | 数据类型：int<br />单位：g*10^6             | /10^6        | 数据类型：float<br />单位：m/s2            |
| 重力加速度 | HDF_SENSOR_TYPE_GRAVITY(257) | 数据类型：int <br />单位：g*10^6            | /10^6        | 数据类型：float<br />单位：m/s2            |
| 计步器     | HDF_SENSOR_TYPE_PEDOMETER(266) | 数据类型：int                               | /1.0         | 数据类型：float                            |
| 陀螺仪     | HDF_SENSOR_TYPE_GYROSCOPE(2) | 数据类型：int <br />单位：rad/s*10^3        | /1000.0      | 数据类型：float<br />单位：rad/s           |
| 地磁计     | HDF_SENSOR_TYPE_MAGNETIC_FIELD(6) | 数据类型：int<br />单位：μT*10^3            | /1000.0      | 数据类型：float <br />单位：μT             |
| 气压计     | HDF_SENSOR_TYPE_BAROMETER(8) | 数据类型：int<br />单位：hpa*100            | /100.0       | 数据类型：float<br />单位：hpa             |
| 温度       | HDF_SENSOR_TYPE_TEMPERATURE(9) | 数据类型：int<br />单位：degrees Celsius*10 | /10.0        | 数据类型：float<br />单位：degrees Celsius |
| 环境光     | HDF_SENSOR_TYPE_AMBIENT_LIGHT(5) | 数据类型：int<br />单位：lux*10^4           | /10000.0     | 数据类型：float<br />单位：lux             |
| 接近光     | HDF_SENSOR_TYPE_PROXIMITY(12) | 数据类型：int                               | 1.0          | 数据类型：float                            |
| 霍尔       | HDF_SENSOR_TYPE_HALL(10) | 数据类型：int                               | 1.0          | 数据类型：float                            |
| 湿度       | HDF_SENSOR_TYPE_HUMIDITY(13) | 数据类型：int                               | /100.0          | 数据类型：float<br />单位：%rh                             |
