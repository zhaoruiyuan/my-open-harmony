# PPG驱动开发

## 概述

​		PPG( Photoplethysmograph )心率计Sensor驱动，对系统进行上电，通过驱动入口，ppg注册到HDF驱动框架，对驱动进行初始化，探测器件在位，并解析配置文件，最后通过ppg差异化代码实现，为上层服务提供稳定的接口能力，对驱动开发者提供开放的接口实现和抽象的配置接口能力。ppg开发主要包括四个部分：设备驱动加载；ppg抽象驱动加载；器件差异化驱动加载；配置文件编写。基于HDF（**H**ardware **D**river **F**oundation）驱动框架的Sensor驱动模型，通过平台解耦、内核解耦，来达到兼容不同内核，统一平台底座的目的，从而帮助开发者实现驱动的“**一次开发、多系统部署**”。



## 开发指导

### 开发步骤

（1）从device info HCS 的Sensor Host里读取Sensor设备管理配置信息，加载并初始化Sensor设备管理驱动。

（2）加载ppg抽象驱动，调用初始化接口，完成Sensor器件驱动的工作项和工作队列的初始化。

（3）从ppg_xxx_config HCS里读取ppg器件差异化驱动配置和私有化配置信息。

（4）ppg差异化驱动，调用通用配置解析接口，完成器件属性信息解析，器件寄存器解析。

（5）ppg差异化驱动完成器件探测，并分配ppg传感器配置资源，完成ppg差异化接口注册。 

（6）ppg器件探测成功之后，差异化驱动通知ppg抽象驱动，注册设备到Sensor设备管理中。

## 开发实例

​		Sensor驱动模型提供标准化的器件驱动，开发者无需独立开发，通过配置即可完成驱动的部署。对驱动模型抽象，屏蔽驱动与不同系统组件间的交互，使得驱动更具备通用性。基于Sensor驱动模型，加载ppg传感器驱动。心率传感器选择通讯接口方式为spi,厂家选择芯海科技型号为CS1262高精度全场景ppg。

1.ppg抽象驱动实现

- ppg抽象驱动在Sensor Host中的配置信息(这部分内容需要按照格式进项编写，添加到对应驱动节点下面，具体格式如下例)

  代码实现路径为

  vendor\hisilicon\hispark_taurus_standard\hdf_config\khdf\device_info\device_info.hcs (L2配置路径)

  具体代码如下:

```hcs
/* ppg传感器设备HCS配置 */
device_sensor_ppg :: device { /* device_sensor_ppg字段是驱动节点名称，自己配置 */
    device0 :: deviceNode {
        policy = 1;                /* policy字段是驱动服务发布的策略，按需配置0，1，2三种策略 */
        priority = 110;            /* ppg驱动启动优先级（0-200），值越大优先级越低，建议默认配100，优先级相同则不保证device的加载顺序 */
        preload = 2;               /* 驱动按需加载字段，0表示加载，2表示不加载 */
        permission = 0664;         /* 驱动创建设备节点权限（默认） */
        moduleName = "HDF_SENSOR_PPG";  /* ppg驱动名称，该字段的值必须和驱动入口结构的moduleName值一致，自己配置 */
        serviceName = "hdf_sensor_ppg"; /* ppg驱动对外发布服务的名称，必须唯一，自己配置 */
     }
}
```

- ppg传感器抽象驱动入口函数实现

  代码实现路径为 drivers\hdf_core\framework\model\sensor\driver\ppg\sensor_ppg_driver.c

  定义ppg抽象驱动对应的HdfDriverEntry对象，其中，Driver  Entry入口函数定义如下：

```c
/*注册ppg传感器入口数据结构体对象*/
struct HdfDriverEntry g_sensorPpgDevEntry = {
    .moduleVersion = 1,                  /*ppg传感器模块版本号*/
    .moduleName = "HDF_SENSOR_PPG", /*ppg传感器模块名，要与device_info.hcs文件里ppg moduleName字段值一样*/
    .Bind = PpgBindDriver,          /*ppg传感器绑定函数*/
    .Init = PpgInitDriver,          /*ppg传感器初始化函数*/
    .Release = PpgReleaseDriver,    /*ppg传感器资源释放函数*/
};

/* 调用HDF_INIT将驱动入口注册到HDF框架中，在加载驱动时HDF框架会先调用Bind函数,再调用Init函数加载该驱动，当Init调用异常时，HDF框架会调用Release释放驱动资源并退出 */
HDF_INIT(g_sensorPpgDevEntry);

```

Bind接口实现驱动接口实例化，实现示例：

```c
int32_t PpgBindDriver(struct HdfDeviceObject *device)
{
    CHECK_NULL_PTR_RETURN_VALUE(device, HDF_ERR_INVALID_PARAM);
    //私有接口分配资源
    struct PpgDrvData *drvData = (struct MagneticDrvData *)OsalMemCalloc(sizeof(*drvData));
   ......
    //需要发布的接口函数
    drvData->ioService.Dispatch = DispatchPpg;
    drvData->device = device;
    device->service = &drvData->ioService;
    g_ppgDrvData = drvData;
    return HDF_SUCCESS;
}
```

Init接口实现驱动接口实例化，实现示例：

```c

int32_t PpgInitDriver(struct HdfDeviceObject *device)
{
	.....
    //工作队列初始化
    if (HdfWorkQueueInit(&drvData->ppgWorkQueue, HDF_PPG_WORK_QUEUE) != HDF_SUCCESS) {
        HDF_LOGE("%s: Ppg init work queue failed", __func__);
        return HDF_FAILURE;
    }
    //工作项初始化
    if (HdfWorkInit(&drvData->ppgWork, PpgDataWorkEntry, drvData) != HDF_SUCCESS) {
        HDF_LOGE("%s: Ppg create thread failed", __func__);
        return HDF_FAILURE;
    }
	......
    return HDF_SUCCESS;
}
```

Release接口在驱动卸载或者Init执行失败时，会调用此接口释放资源：

```c
void PpgReleaseDriver(struct HdfDeviceObject *device)
{
    CHECK_NULL_PTR_RETURN(device);

    struct PpgDrvData *drvData = (struct PpgDrvData *)device->service;
    CHECK_NULL_PTR_RETURN(drvData);

    //销毁工作和工作队列资源
    HdfWorkDestroy(&drvData->ppgWork);
    HdfWorkQueueDestroy(&drvData->ppgWorkQueue);
    
    ReleasePpgCfgData();
    (void)memset_s(drvData, sizeof(struct PpgDrvData), 0, sizeof(struct PpgDrvData));
    OsalMemFree(drvData);
}
```

2.ppg传感器差异化驱动实现

- ppg差异化驱动在Sensor Host中的配置信息

  代码实现路径为

  vendor\hisilicon\hispark_taurus_standard\hdf_config\khdf\device_info\device_info.hcs (L2配置路径)

```c
/* ppg CS1262传感器设备HCS配置 */
device_sensor_cs1262 :: device {
    device0 :: deviceNode {
        policy = 1; /* policy字段是驱动服务发布的策略 */
        priority = 120; /* ppg CS1262驱动启动优先级（0-200），值越大优先级越低，建议默认配100，优先级相同则不保证device的加载顺序 */
        preload = 2; /* 驱动按需加载字段，0表示加载，2表示不加载 */
        permission = 0664; /* 驱动创建设备节点权限 */
        moduleName = "HDF_SENSOR_PPG_CS1262"; /* ppg CS1262驱动名称，该字段的值必须和驱动入口结构的moduleName值一致 */
        serviceName = "hdf_ppg_cs1262"; /* ppg CS1262驱动对外发布服务的名称，必须唯一 */
        deviceMatchAttr = "hdf_sensor_ppg_cs1262_driver"; /* ppg CS1262驱动私有数据匹配的关键字，必须和驱动私有数据配置表中match_attr值相等 */
     }
}
```

ppg差异化驱动私有HCS配置

代码路径如下

vendor\hisilicon\hispark_taurus_standard\hdf_config\khdf\sensor\ppg\ppg_cs1262_config.hcs

```c
#include "../sensor_common.hcs"
root {
        ppg_cs1262_chip_config : sensorConfig {
            match_attr = "hdf_sensor_ppg_cs1262_driver";
            sensorInfo :: sensorDeviceInfo {
                sensorName = "ppg";             // 传感器名称
                vendorName = "chipsea_cs1262";  // 供应商名称
                sensorTypeId = 129;             // 传感器类型ID号：参考（enum SensorTypeTag）结构体定义
                sensorId = 129;                 // 传感器 ID
            }
            sensorBusConfig :: sensorBusInfo {
                busType = 1;                    // 通讯方式 0:i2c 1:spi
                busNum = 1;                     // 总线地址
                busAddr = 0;                 // 外设地址
                regWidth = 2;                   // 外设寄存器长度 2 byte
                csNum = 0;
                spiBusCfg :: spiBusInfo { /* reference struct SpiCfg */
                maxSpeedHz = 8388608; //8 * 1024 * 1024;
                mode = 0;
                transferMode = 1;
                bitsPerWord = 8;
            }
                
            }
            sensorIdAttr :: sensorIdInfo  {
                chipName = "cs1262";            // 器件名称
                chipIdRegister = 0x01f1 ;       // 器件探测地址
                chipIdValue = 0x1262;           // 器件对应探测地址存储的数据
            }
             ppgPinConfig {
                /*
                 * regAddr: register address
                 * regLen: with of regAddr
                 * value: config register value
                 */
                multiUseSet = [
                    0x112F0020, 0x4, 0x4f1,
                    0x112F0024, 0x4, 0x4f1,
                    0x112F0028, 0x4, 0x4f1,
                    0x112F002C, 0x4, 0x501
                ];
                /*
                    GPIOINDEX 0,1,2,
                    GPIONO 8*3+5 = 29, for gpio3_5 of hi3516dev300
                    GPIOTYPE GPIO(0)\INT(1)
                    DIR: IN(0)\OUT(1)
                    MODE: LOW(0)\HIGH(1)
                          irq trigger edge, rising(1)\falling(2)\high(4)\low(8)\thread
                 */
                gipos = [
                    0, 29, 0, 1, 1,
                    1, 6,  1, 0, 1,
                ];
            }
        }
}

```

- ppg传感器差异化驱动入口函数实现

  代码实现路径为drivers\peripheral\sensor\chipset\ppg\ppg_cs1262.c

  定义ppg差异化驱动对应的HdfDriverEntry对象，其中，Driver  Entry入口函数定义如下：

```c
/*注册ppg传感器CS1262口数据结构体对象*/
struct HdfDriverEntry g_ppgCs1262DevEntry = {
  .moduleVersion = CS1262_MODULE_VER, /*ppg传感器模块版本号*/
  .moduleName = "HDF_SENSOR_PPG_CS1262",  /*ppg传感器CS1262模块名，要与device_info.hcs文件里 moduleName字段值一样*/
  .Bind = Cs1262BindDriver,/*ppg传感器CS1262绑定函数*/
  .Init = Cs1262InitDriver, /*ppg传感器CS1262初始化函数*/
  .Release = Cs1262ReleaseDriver /*ppg传感器CS1262资源释放函数*/
};

/* 调用HDF_INIT将驱动入口注册到HDF框架中，在加载驱动时HDF框架会先调用Bind函数,再调用Init函数加载该驱动，当Init调用异常时，HDF框架会调用Release释放驱动资源并退出 */
HDF_INIT(g_ppgCs1262DevEntry);
```

Bind驱动接口实例化，实现示例：

```c
int32_t Cs1262BindDriver(struct HdfDeviceObject *device)
{
    CHECK_NULL_PTR_RETURN_VALUE(device, HDF_ERR_INVALID_PARAM);

    struct Cs1262DrvData *drvData = (struct Cs1262DrvData *)OsalMemCalloc(sizeof(*drvData));
    CHECK_NULL_PTR_RETURN_VALUE(drvData, HDF_ERR_MALLOC_FAIL);

    drvData->ioService.Dispatch = DispatchCs1262;
    drvData->device = device;
    device->service = &drvData->ioService;
    g_cs1262DrvData = drvData;

    return HDF_SUCCESS;
}
```

Init驱动接口实例化，实现示例：

```c
int32_t Cs1262InitDriver(struct HdfDeviceObject *device)
{
    int32_t ret;
    CHECK_NULL_PTR_RETURN_VALUE(device, HDF_ERR_INVALID_PARAM);
    struct Cs1262DrvData *drvData = (struct Cs1262DrvData *)device->service;
    CHECK_NULL_PTR_RETURN_VALUE(drvData, HDF_ERR_INVALID_PARAM);
    //设备基本配置信息解析
    ret = ParsePpgCfgData(device->property, &(drvData->ppgCfg));
    if ((ret != HDF_SUCCESS) || (drvData->ppgCfg == NULL)) {
        HDF_LOGE("%s: cs1262 construct fail!", __func__);
        return HDF_FAILURE;
    }
	//ppg总线配置
    ret = Cs1262InitSpi(&drvData->ppgCfg->sensorCfg.busCfg);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: cs1262 init spi!", __func__);
        return HDF_FAILURE;
    }

    ret = CheckChipId(drvData->ppgCfg);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: cs1262 check chip fail!", __func__);
        return HDF_FAILURE;
    }
	//器件探测
    ret = InitChip();
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: cs1262 init chip fail!", __func__);
        return HDF_FAILURE;
    }

    struct PpgChipData chipData = {
        .cfgData = drvData->ppgCfg,
        .opsCall = {
            .ReadData = Cs1262ReadData,
            .Enable = Cs1262Enable,
            .Disable = Cs1262Disable,
            .SetOption = Cs1262SetOption,
            .SetMode = Cs1262SetMode,
        }
    };
	//注册差异化接口
    ret = RegisterPpgChip(&chipData);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: Register CS1262 failed", __func__);
        return HDF_FAILURE;
    }

    HDF_LOGI("%s: cs1262 init driver success", __func__);
    return HDF_SUCCESS;
}
```

Release驱动接口实例化，实现示例：

```c
void Cs1262ReleaseDriver(struct HdfDeviceObject *device)
{
    CHECK_NULL_PTR_RETURN(device);
    struct Cs1262DrvData *drvData = (struct Cs1262DrvData *)device->service;
    CHECK_NULL_PTR_RETURN(drvData);
    CHECK_NULL_PTR_RETURN(drvData->ppgCfg);

    Cs1262ReleaseSpi(&drvData->ppgCfg->sensorCfg.busCfg);
    (void)memset_s(drvData, sizeof(struct Cs1262DrvData), 0, sizeof(struct Cs1262DrvData));
    OsalMemFree(drvData);
}
```

ppg差异化函数接口实现示例

需要开发这实现的Cs1262ReadData接口函数，在Cs1262InitDriver函数里面注册此函数。

```c
int32_t Cs1262ReadData(uint8_t *outBuf, uint16_t outBufMaxLen, uint16_t *outLen)
{
    uint8_t ret;
    uint16_t ifr = 0;
    CHECK_NULL_PTR_RETURN_VALUE(outBuf, HDF_ERR_INVALID_PARAM);

    ret = Cs1262ReadRegs(CS1262_IFR_REG, &ifr, 1);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: Cs1262ReadRegs CS1262_IFR_REG fail", __func__);
        return HDF_FAILURE;
    }

    if (ifr & IFR_RDY_FLAG) {
        ret = ReadFifo(outBuf, outBufMaxLen, outLen);
        if (ret != HDF_SUCCESS) {
            HDF_LOGE("%s: ReadFifo fail", __func__);
        }

        ret = ClearIFR(FIFO_RDY_IFR_OFFSET);
        if (ret != HDF_SUCCESS) {
            HDF_LOGE("%s: ClearIFR fail", __func__);
            return HDF_FAILURE;
        }
    }
    return HDF_SUCCESS;
}
```

## 编译指导

在编译的过程中，首先需要在Sensor Host中的配置信息中设置具体器件是否要驱动加载，修改preload字段属性值。

代码路径为

vendor\hisilicon\hispark_taurus_standard\hdf_config\khdf\device_info\device_info.hcs

```c
device_sensor_ppg :: device {
    device0 :: deviceNode {
        policy = 1;
        priority = 110;
        preload = 0;
        permission = 0664;
        moduleName = "HDF_SENSOR_PPG";
        serviceName = "hdf_sensor_ppg";
    }
}
device_ppg_cs1262 :: device {
    device0 :: deviceNode {
        policy = 1;
        priority = 120;
        preload = 0;
        permission = 0664;
        moduleName = "HDF_SENSOR_PPG_CS1262";
        serviceName = "hdf_ppg_cs1262";
        deviceMatchAttr = "hdf_sensor_ppg_cs1262_driver";
    }
}
```

传感器驱动实现在内核态，代码参与编译通过适配makefile实现，并通过内核模块宏定义，控制ppg设备驱动是否参与编译。

代码路径为

drivers\adapter\khdf\linux\model\sensor\Makefile

```c
......
obj-$(CONFIG_DRIVERS_HDF_SENSOR_PPG) += $(SENSOR_ROOT_DIR)/ppg/sensor_ppg_driver.o \
                $(SENSOR_ROOT_DIR)/ppg/sensor_ppg_config.o

obj-$(CONFIG_DRIVERS_HDF_SENSOR_PPG_CS1262) += $(SENSOR_ROOT_CHIPSET)/chipset/ppg/ppg_cs1262_spi.o \
                $(SENSOR_ROOT_CHIPSET)/chipset/ppg/ppg_cs1262.o \
                $(SENSOR_ROOT_CHIPSET)/chipset/ppg/ppg_cs1262_fw.o
.....
-I$(srctree)/drivers/hdf/peripheral/sensor/chipset/ppg \
-I$(srctree)/drivers/hdf/framework/model/sensor/driver/ppg \
......
```

ppg驱动加载，将CONFIG_DRIVERS_HDF_SENSOR_PPG;  CONFIG_DRIVERS_HDF_SENSOR_PPG_CS1262宏字段设置为y即可。

ppg驱动不加载，将CONFIG_DRIVERS_HDF_SENSOR_PPG;  CONFIG_DRIVERS_HDF_SENSOR_PPG_CS1262宏字段设置为is not set即可。

代码路径为

kernel\linux\config\linux-5.10\arch\arm\configs\hispark_taurus_standard_defconfig

```
......
# CONFIG_DRIVERS_HDF_SENSOR_ACCEL is not set
# CONFIG_DRIVERS_HDF_SENSOR_GYRO is not set
# CONFIG_DRIVERS_HDF_SENSOR_PPG is not set
# CONFIG_DRIVERS_HDF_SENSOR_PPG_CS1262 is not set
CONFIG_DRIVERS_HDF_STORAGE=y
CONFIG_DRIVERS_HDF_VIBRATOR=y
.....
```

