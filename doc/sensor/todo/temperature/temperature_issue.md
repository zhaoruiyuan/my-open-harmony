# 温度传感器需求issue要求

## 需求标题
**温度传感器设备驱动开发**

## 场景描述
- 面向全场景、全连接、全智能时代，智能终端设备通过不同硬件厂家的环境温度传感器探测环境温度，通过标准的HDI接口，提供温度传感器控制和数据上报能力。
## 需求描述
- 基于HDF驱动框架和sensor传感器设备驱动模型，开发温度传感器设备驱动模型驱动，实现温度传感器硬件信息查询，传感器使能，去使能，采样率下发，数据上报能力。
- 基于温度传感器设备驱动模型，适配两款不同器件厂家器件驱动，完成不同器件厂家驱动能力接入。

## 需求规格
- 1.支持数据订阅和采样率配置
- 2.支持温度传感器使能，去使能
- 3.支持温度值数据上报，温度单位为摄氏度
- 4.支持上报数据时间产生的时间戳（单位ns）
- 5.支持数据模式上报（数据改变上报模式）

## 需求产出和验收标准
- 基于HDF驱动框架和sensor传感器设备驱动模型，交付温度传感器设备驱动模型驱动，完成不同厂家温度传感器适配接入能力。
- 基于温度传感器设备模型驱动，完成至少2个器件厂家器件驱动开发，具有温度传感器硬件信息查询，传感器使能，去使能，采样率下发，相对温度数据上报能力。
- 上报温度传感器信息和温度数据格式，按照已有的HDI接口和数据结构定义适配。
- 开发的温度传感器驱动通过OpenHarmony HATS sensor模块认证。
- 交付需求分析文档，温度传感器功能详细设计，自测试功能用例，温度设备驱动开发指南，维护手册文档
## 需求难度
- 中
## 技术要求
- 了解OpenHarmony项目工程基本技术架构，熟悉HDF驱动框架基本知识，Sensor设备驱动模型知识。
- 熟悉传感器基础知识，能够阅读传感器器件手册
- 熟悉掌握Linux内核及硬件驱动等知识
- 熟悉git代码版本管理工具
## 计划完成时间
- 2023年5月30日
## 认领单位/个人
- xxx
## 联系方式
- xxx

## 技术参考文档
- [sensor设备驱动开发指南](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/driver/driver-peripherals-sensor-des.md)
- [HDF驱动介绍](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/driver/driver-overview-foundation.md)
- [HDF驱动框架介绍(HDF驱动框架章节)](https://gitee.com/openharmony/docs/tree/master/zh-cn/device-dev/driver#/openharmony/docs/blob/master/zh-cn/device-dev/driver/driver-hdf-development.md)
- [sensor HDI接口定义](https://gitee.com/openharmony/drivers_interface/tree/master/sensor/v1_0)
- [sensor数据单位介绍](https://gitee.com/Kevin-Lau/my-open-harmony/blob/master/doc/sensor/Sensor%E6%95%B0%E6%8D%AE%E6%A0%BC%E5%BC%8F%E8%BD%AC%E6%8D%A2%E6%A0%87%E5%87%86.md)