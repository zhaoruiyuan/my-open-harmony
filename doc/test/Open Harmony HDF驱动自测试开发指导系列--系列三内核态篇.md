# Open Harmony HDF驱动自测试开发指导系列--系列三内核态篇

 

- [1、内核态自测试用例框架介绍](##1、内核态自测试用例框架介绍)
- [2、内核态自测试用例开发步骤](##2、内核态自测试用例开发步骤)
  - [2.1用户态测试套](##2.1用户态测试套)
  - [2.2适配HDF框架](###2.2适配HDF框架)
  - [2.3内核态测试用例实现](###2.3内核态测试用例实现)
  - [2.4测试驱动的实现](###2.4测试驱动的实现)
  - [2.5编译脚本适配](###2.5编译脚本适配)
  - [2.6 hcs配置](###2.6 hcs配置)
  - [2.7依赖的资源文件配置](###2.7依赖的资源文件配置)

- [3、用例测试](##3、用例测试)




## 1、内核态自测试用例框架介绍

​        内核态自测试用例的框架由于需要测试内核态用例，采用了用户态自测试框架+HDF设备管理+内核态的自测试用例框架，开发一个内核态测试用例需要写用户态的测试套，然后通过hdf消息交互机制的方式，用户态和内核态进行交互，调用内核态测试入口函数（HdfxxxEntry），通过cmd分发方式，调用各个子模块的测试用例。

## 2、内核态自测试用例开发步骤

​        以标准系统中Hi3516DV300产品为例，介绍内核态测试用例开发。

### 2.1用户态测试套

​        用户态测试套的路径为HDF驱动功能测试时，一般外设测试套定义路径为drivers\peripheral仓各模块test目录下（例如drivers\peripheral\audio\test\unittest\common），HDF框架功能测试套定义路径为drivers\framework仓下各模块test目录下(例如support\platform\test\unittest\common)。

​        测试套文件需要继承testing::Test类，命名以测试模块+test.cpp进行命名，来定义测试套文件。具体可参考hdf_rtc_entry_test.cpp来实现。

​        引用gtest头文件和ext命名空间，具体实现如下示例所示：

```
#include <gtest/gtest.h>
using namespace testing::ext;
```

​        实现测试套时需要先进行预处理和后处理操作如下图所示，SetUpTestCase是针对该测试套下所有的用例执行前的预置处理，需要调用HdfTestOpenService()函数；TearDownTestCase是针对该测试套下所有的用例执行后释放，需要调用HdfTestOpenService()函数。 SetUp和TearDown是针对该测试套下每条用例执行前和执行后的处理逻辑。具体实现可参照hdf_rtc_entry_test.cpp。

```
void HdfRtcTest::SetUpTestCase()
{
  // 获取测试服务，并分配测试资源
   HdfTestOpenService(); 
    ......
}
void HdfRtcTest::TearDownTestCase()
{
    // 释放测试服务，并释放测试资源
    ......
    HdfTestCloseService();
}
void HdfRtcTest::SetUp()
{
}
void HdfRtcTest::TearDown()
{
}
```

​        针对被测对象的特性编写测试用例，以hdf_rtc_entry_test.cpp文件为例说明。

用例注释是在编写测试用例时，先要对以下信息进行备注：

@tc.name：用例名称 

@tc.desc：描述用例详细描述，包括测试目的、测试步骤、期望结果等。

@tc.type：测试属性分类（FUNC（功能测试）、PERF（性能测试）、SECU（安全测试）、RELI（可靠性测试））。

@tc.require：需求编号 

​        逻辑实现：调用测试套HWTEST_F编写测试用例（Gtest框架中常用的测试套种类及功能请参考[测试用例开发指导](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-testguide-test.md)的开发指导篇章中的接口说明章节）。测试用例的预期结果必须要有对应的断言，来判断测试结果是否与预期结果一致，（常用断言功能请参考[Open Harmony HDF驱动自测试开发指导系列--系列一测试框架篇.docx](Open Harmony HDF驱动自测试开发指导系列--系列一测试框架篇.docx)的测试用例介绍篇章中的常用断言介绍章节），如EXPECT_EQ。HWTEST_F测试套编写的用例具备独立性，执行过程相互之间不受影响。通过调用HdfTestSendMsgToService，将消息通过cmd分发方式发送到内核态，调用内核态测试用例进行测试。

```
// pal rtc test case number
enum HdfRtcTestCaseCmd {
RTC_WR_TIME,
.......
};

/**
  * @tc.name: testRtcReadWriteTime001
  * @tc.desc: rtc function test
  * @tc.type: FUNC
  * @tc.require: xxxx
  */
HWTEST_F(HdfRtcTest, testRtcReadWriteTime001, TestSize.Level1)
{
    struct HdfTestMsg msg = { TEST_PAL_RTC_TYPE, RTC_WR_TIME, -1 };
    EXPECT_EQ(0, HdfTestSendMsgToService(&msg));
```

​        模块命令字TEST_PAL_RTC_TYPE需在drivers/framework/test/unittest/include目录下的haf_uhdf_test.h中进行定义，如下示例所示，每个模块100，模块中子模块按序排列，注意此处定义要与内核态hdf_main_test.h中的定义一致，否则无法进行通讯。

```
enum HdfTestSubModuleCmd {
    ......
    TEST_PAL_RTC_TYPE       = 19,
    TEST_PAL_ADC_TYPE       = 20,
    ......
#define HDF_OSAL_TEST_ITEM(v) (TEST_OSAL_BEGIN + (v))
    TEST_OSAL_ITEM = HDF_OSAL_TEST_ITEM(1),
    TEST_USB_HOST_TYPE      = 1000,
TEST_USB_HOST_RAW_TYPE  = 1100,
......
};
```

- 测试套编译脚本

  1.编译脚本路径：drivers/adapter/ubdf中的每个模块的test/unittest目录下的BUILD.gn中，sources中新增测试套路径,如下示例所示：

```
sources = [
      ......
      "//drivers/framework/support/platform/test/unittest/common/hdf_spi_test.cpp",
      "//drivers/framework/support/platform/test/unittest/common/hdf_i2s_test.cpp",
      ....
]
```

​        2.添加依赖测试框架用户态库libhdf_test_common.z.so路径为drivers/adapter/ubdf中的每个模块的test/unittest目录下的BUILD.gn中，具体实现如下示例所示。

```
deps = [
      ......
      "//drivers/adapter/uhdf/test/unittest/common:hdf_test_common",
      ......
  ]
```

​        ***\*注意：\****依赖测试框架用户态库libhdf_test_common.z.so的编译输出路径为out\ohos-arm-release\tests\unittest\hdf中的对应模块下resource\hdf\hdf目录下。

​        如果手动测试时，需要将此文件push到设备system/lib目录下，如果用xdevice自测试工具，只需要配置资源xml文件，具体实现参考[测试子系统](https://gitee.com/openharmony/docs/blob/master/zh-cn/readme/测试子系统.md))中的使用测试框架及测试结果与日志章节。

​        3.编译脚本入口适配路径：\drivers\adapter\uhdf2路径下的test目录下的ohos.build文件。每新增一个模块的测试用例编译脚本，都需在此文件中的"test_list"下新增脚本入口，如下示例所示：

```
{
  "subsystem": "hdf",
  "parts": {
    "hdf": {
      "module_list": [
        ......
      ],
      "test_list": [
        "//drivers/peripheral/wlan/test:hdf_test_wifi",
        ......
      ]
    }
  }
}
```

### 2.2适配HDF框架

​        模块入口路径为drivers/framework/test/unittest/common下的hdf_main_test.c文件，用模块宏隔离里面的每个入口函数，其他的不用修改。

```
#if defined(LOSCFG_DRIVERS_HDF_PLATFORM_RTC) || defined(CONFIG_DRIVERS_HDF_PLATFORM_RTC)
#include "hdf_rtc_entry_test.h"
#endif

#if defined(LOSCFG_DRIVERS_HDF_PLATFORM_RTC) || defined(CONFIG_DRIVERS_HDF_PLATFORM_RTC)
    { TEST_PAL_RTC_TYPE, HdfRtcEntry },
#endif
```

​        在drivers/framework/test/unittest/common目录下的hdf_main_test.h文件中定义模块命令字TEST_PAL_RTC_TYPE，注意此处定义要与用户态drivers/framework/test/unittest/include路径下的haf_uhdf_test.h中的定义一致，否则无法进行通讯。

### 2.3内核态测试用例实现

​        内核态测试用例的路径为drivers/framework仓下的test/unittest路径下每个模块中的common目录下。命名以测试模块+test.c进行命名。

​        内核态测试用例，每个用例都需要封装成int xxx（void）型，成功为0，失败为-1，以 RtcTestInit用例为例进行逻辑实现。

```
static int32_t RtcTestInit(void)
{
    g_rtcHandle = RtcOpen();
    if (g_rtcHandle == NULL) {
        HDF_LOGE("RtcTestInit: g_rtcHandle NULL");
        return -1;
    }
    return 0;
}
```

​         通过调用入口函数（以hdfRtcEntry为例），将用户态发送过来的消息在内核态进行测试，再将测试结果返回到用户态。

```
// add test case entry
HdfTestCaseList g_hdfRtcTestCaseList[] = {
{ RTC_WR_TIME, RtcReadWriteTime },
......
}；
int32_t HdfRtcEntry(HdfTestMsg *msg)
{
    ......
for (i = 0; i < sizeof(g_hdfRtcTestCaseList) / sizeof(g_hdfRtcTestCaseList[0]); ++i) {
       ........
    }
    return HDF_SUCCESS;
}
```

### 2.4测试驱动的实现

​         对于一些产品或硬件差异较大的模块，需要写测试驱动，具体实现可参考此路径为drivers\framework\test\unittest\platform\common目录下i2c_driver_test.c。

### 2.5编译脚本适配

​        在drivers/adapter/linux/test目录下的Makefile文件，每新增一个测试套文件，需要在此目录下适配其编译脚本，具体实现如下示例所示：

```
obj-$(CONFIG_DRIVERS_HDF_PLATFORM_RTC) += $(HDF_FRAMWORK_TEST_ROOT)/platform/hdf_rtc_entry_test.o
obj-$(CONFIG_DRIVERS_HDF_PLATFORM_SPI) += $(HDF_FRAMWORK_TEST_ROOT)/platform/common/spi_test.o \
```

​        在drivers/adapter/linux/test目录下的test_khdf.mk文件，每新增一个测试套文件，需要在此目录下适配其编译路径，具体实现如下示例所示：

```
-I$(HDF_FRAMEWORK_TEST_ROOT)/sensor \
    -I$(HDF_FRAMEWORK_ROOT)/model/sensor/driver/include \
    -I$(HDF_FRAMEWORK_ROOT)/model/sensor/driver/common/include \
```

​         注：测试用例编译配置规范：测试用例采用GN方式编译，配置遵循本开源项目[编译构建](https://gitee.com/openharmony/build)。

### 2.6 hcs配置

​        以标准系统hcs路径进行描述。

​        对于一些产品或硬件差异较大的模块，需要写测试驱动，则需要在此路径下vendor\hisilicon\Hi3516DV300\hdf_config\khdf\hdf_test新建xxx_test_config.hcs文件，具体实现可参考i2c_test_config.hcs，如下示例所示：

```
root {
    module = "i2c";
    i2c_config {
        i2c_controller_tester {
            match_attr = "I2C_TEST";
            bus_num  = 6;
            dev_addr = 0x5d;
            reg_addr = 0xd0;
            reg_len = 2;
            buf_size = 7;
        }
    }
}
```

### 2.7依赖的资源文件配置

​        测试依赖的配置文件放置路径\drivers\adapter\uhdf2\test\resource中各模块目录下ohos_test.xml文件，具体实现可参考同目录下的其他文件，如下示例所示：

```
<configuration ver="2.0">
    <target name="hdf_adapter_uhdf_test_platform"> // 测试单元的名字
        <preparer>
            <option name="push" value="hdf/hdf/libhdf_test_common.z.so -> /system/lib" src="out"/> // 编译输出后测试资源位于out/release/$(子系统名)目录
        </preparer>
    </target>
</configuration>
```



## 3、用例测试

​        具体测试步骤可参考[hdf自测试介绍](https://gitee.com/Kevin-Lau/my-open-harmony/wikis/hdf自测试介绍?sort_id=4409313)。