# Open Harmony HDF驱动自测试开发指导系列--系列一测试框架篇

 

- [1、HDF测试框架简介](##1、HDF测试框架简介)
- [2、HDF测试框架介绍](##2、HDF测试框架介绍)
  - [2.1测试框架接口能力介绍](###2.1测试框架接口能力介绍)
  - [2.2 Gtest测试框架接口能力介绍](##2.2Gtest测试框架接口能力介绍)

- [3、HDF测试编译文件介绍](##3、HDF测试编译文件介绍)
  - [3.1测试驱动编译入口介绍](###3.1测试驱动编译入口介绍)
  - [3.2测试用例编译文件介绍](###3.2测试用例编译文件介绍)

- [4、资源配置文件介绍](##4、资源配置文件介绍)
- [5、测试](##5、测试)

 

## 1、HDF测试框架简介

​        根据HDF驱动子系统驱动组件化和按需加载的优势，构建Gtest+HDF差异化配置自测试框架，支持模块用例可裁剪，产品差异化配置，多平台，多量级高效适配。HDF测试框架支持场景测试，功能测试，模块级测试，安全功能测试等，让测试关注“客户需求与感知”。

​        HDF自测试框架采用Gtest用户界面，结合虚拟HDF测试驱动和差异化配置的测试框架，有以下优点：

（1）HDF Gtest的测试框架使开发者只需关注模块功能用例，不用关注用户界面；
（2）采用虚拟HDF测试驱动，使开发者只需关注模块用例适配，不用关注用户态和内核态交互；
（3）支持HDF的HCS差异化配置，屏蔽产品差异，高效适配多产品多平台。

## 2、HDF测试框架介绍

​        HDF测试框架包括两部分：用户态测试部分和内核态测试部分，框架图如图1所示。 

![hdf-frame](ipic/hdf-frame.png)                                                                                                        图1

调用测试用例进行测试时，分为以下几步：

（1）在界面输入命令；

（2）调用用户态测试套；

（3）通过用户态测试套将测试消息经过common发送到内核态；

（4）执行内核态测试用例；

（5）对执行结果进行校验；

（6）将测试结果发送到用户态。

其流程如图2所示：

![hdf-tree](ipic/hdf-tree.png)

​                                                                                                                 图2

### 2.1测试框架接口能力介绍

​        HDF测试框架对外提供用户态和内核态测试接口能力。提供用户态框架抽象接口，获取测试框架服务和分配测试资源接口，释放测试框架服务和释放测试资源接口，发送测试命令字消息抽象接口。

- 用户态接口介绍

​      接口定义实现路径为drivers\framework\test\unittest\common\hdf_common_test.c

​      接口功能说明：

```
void HdfTestOpenService(void)
{
    // 获取测试框架服务
    g_testService = HdfIoServiceBind(HDF_TEST_SERVICE_NAME);
    g_msg = HdfSBufObtainDefaultSize();
    if (g_msg == NULL) {
        printf("fail to obtain sbuf data\n\r");
        return;
    }
    // 分配测试资源接口
    g_reply = HdfSBufObtainDefaultSize();
    if (g_reply == NULL) {
        printf("fail to obtain sbuf reply\n\r");
        HdfSBufRecycle(g_msg);
        return;
    }
}

void HdfTestCloseService(void)
{
    // 释放测试资源接口
    if (g_msg != NULL) {
        HdfSBufRecycle(g_msg);
        g_msg = NULL;
    };
    if (g_reply != NULL) {
        HdfSBufRecycle(g_reply);
        g_reply = NULL;
    };
    // 释放测试框架服务
    if (g_testService != NULL) {
        HdfIoServiceRecycle(g_testService);
        g_testService = NULL;
    };
}

// 发送测试命令字消息抽象接口
int HdfTestSendMsgToService(struct HdfTestMsg *msg)
{
    ......
    // 写消息
    if (!HdfSbufWriteBuffer(g_msg, msg, sizeof(*msg))) {
        printf("HdfTestSendMsgToService g_msg write failed\n\r");
    }
    // 发送消息
    ret = g_testService->dispatcher->Dispatch(&g_testService->object, 0, g_msg, g_reply);
    if (ret != HDF_SUCCESS) {
        printf("HdfTestSendMsgToService fail to send service call\n\r");
        return ret;
    }
    ......
    }
    // 结果校验
    if (testReply == NULL) {
        printf("HdfTestSendMsgToService testReply is null\n\r");
        ret = -1;
    } else {
          ret = testReply->result;
    }
    HdfSbufFlush(g_msg);
    HdfSbufFlush(g_reply);
    ......
}
```

​        对外提供接口能力定义路径为drivers\framework\test\unittest\include\hdf_uhdf_test.h，此文件中是对模块命令字的定义，如下示例所示：

```
// two hundred values allocated per submodule
enum HdfTestSubModuleCmd {
    ......
    TEST_PAL_SPI_TYPE       = 1,
    TEST_PAL_GPIO_TYPE      = 2,
    ......
    TEST_PAL_END            = 200,
    TEST_OSAL_BEGIN = TEST_PAL_END,
#define HDF_OSAL_TEST_ITEM(v) (TEST_OSAL_BEGIN + (v))
    TEST_OSAL_ITEM = HDF_OSAL_TEST_ITEM(1),
    ......
    TEST_HDF_FRAME_END      = 800,
    TEST_USB_DEVICE_TYPE    = 900,
   ......
};
```

- 内核态接口介绍


​        接口定义实现路径：drivers\framework\test\unittest\common\hdf_main_test.c。

​        接口能力说明如下：

主要侵入式修改g_hdfTestFuncList数组

```
HdfTestFuncList g_hdfTestFuncList[] = {
......
#if defined(LOSCFG_DRIVERS_HDF_PLATFORM_ADC) || defined(CONFIG_DRIVERS_HDF_PLATFORM_ADC)
    { TEST_PAL_ADC_TYPE, HdfAdcTestEntry },
#endif
......
};
```

虚拟的测试框架驱动

```
struct HdfDriverEntry g_hdfTestDevice = {
    .moduleVersion = 1,
    .moduleName = "HDF_TEST",
    .Bind = HdfTestDriverBind,
    .Init = HdfTestDriverInit,
    .Release = HdfTestDriverRelease,
};
```

​         为了与用户态进行正常通讯，需要在此文件中对模块命令字进行定义，路径为drivers\framework\test\unittest\common\hdf_main_test.h，此处定义要与drivers\framework\test\unittest\include\hdf_uhdf_test.h定义保持一致。

## 2.2 Gtest测试框架接口能力介绍

- 在Gtest测试框架中，常用的测试套有三种：

HWTEST(calculatorAddTest, Testpoint_001, TestSize.Level1):用例执行无SetUp、TearDown依赖，只适合做简单的测试，用例之间可能会相互影响，不具备独立性。

HWTEST_F(calculatorAddTest, Testpoint_001, TestSize.Level1):用例执行依赖SetUp、TearDown，具备独立性，执行过程之间不受影响。

HWTEST_P(calculatorAddTest, Testpoint_001, TestSize.Level1):参数化测试，测试逻辑与数据分离，测试类须继承于testing::TestWithParam并指定需要验证的目标数据结构，对过程结果的判断不够清晰。

- 在Gtest测试框架中，断言有两类ASSERT和EXPECT。

ASSERT_*:致命失败，遇错中断；

EXPECT_*:非致命失败，遇错跳过继续执行。

根据功能，又将上面断言分为三类：

（1）条件断言：ASSERT_TRUE(condition)/EXPECT_TRUE(condition):验证为condition is true。

（2）数值型数据断言：

ASSERT_EQ(Val1, Val2)/EXPECT_EQ(Val1, Val2):验证为Val1 == Val2；

ASSERT_NE(Val1, Val2)/EXPECT_NE(Val1, Val2):验证为Val1 != Val2；

ASSERT_LT(Val1, Val2)/EXPECT_LT(Val1, Val2):验证为Val1 < Val2；

ASSERT_LE(Val1, Val2)/EXPECT_LE(Val1, Val2):验证为Val1 <= Val2；

ASSERT_GT(Val1, Val2)/EXPECT_GT(Val1, Val2):验证为Val1 > Val2；

ASSERT_GE(Val1, Val2)/EXPECT_GE(Val1, Val2):验证为Val1 >= Val2；

（3）字符串断言：

ASSERT_STREQ(str1, str2)/EXPECT_STREQ(str1, str2):验证为：the two C strings have the same content；

ASSERT_STRNE(str1, str2)/EXPECT_STRNE(str1, str2):验证为：the two C strings have the different content；

ASSERT_STRCASEEQ(str1, str2)/EXPECT_STRCASEEQ(str1, str2):验证为：the two C strings have the same content ignoring case；

ASSERT_STRCASENE(str1, str2)/EXPECT_STRCASENE(str1, str2):验证为：the two C strings have the different content ignoring case；

## 3、HDF测试编译文件介绍

### 3.1测试驱动编译入口介绍

​        测试驱动编译入口路径：drivers\adapter\khdf\linux\test\Makefile，测试驱动编译入口文件具体内容如下示例所示，新增的测试用例需要在此处添加其编译入口，编译的生成到IMAGE镜像里。

```
include drivers/hdf/khdf/test/test_khdf.mk
HDF_FRAMWORK_TEST_ROOT = ../../../../framework/test/unittest
obj-y  += $(HDF_FRAMWORK_TEST_ROOT)/common/hdf_main_test.o \
          $(HDF_FRAMWORK_TEST_ROOT)/osal/osal_test_entry.o \
          ......
obj-$(CONFIG_DRIVERS_HDF_SENSOR) += $(HDF_FRAMWORK_TEST_ROOT)/sensor/hdf_sensor_test.o
......
ccflags-y += -Idrivers/hdf/framework/include \
             -Idrivers/hdf/framework/include/osal \
            ......
```

### 3.2测试用例编译文件介绍

​         HDF驱动功能测试时，一般外设测试用例的编译文件在drivers\peripheral仓各模块test目录下（例如drivers\peripheral\audio\test\unittest），HDF框架功能测试用例的编译文件在drivers\adapter仓下uhdf\test\unittest路径各模块目录下的BUILD.gn(例如drivers\adapter\uhdf\test\unittest\common\BUILD.gn)。

​        以drivers\adapter\uhdf\test\unittest\platform\BUILD.gn为例进行说明。

```
import("//build/lite/config/test.gni") // 导入测试用例编译模板文件
// 指定测试用例编译目标输出的文件名称
unittest("hdf_adapter_uhdf_test_config") {   
  output_extension = "bin"
  output_dir = "$root_out_dir/test/unittest/hdf" // 指定测试用例文件的输出路径。
  include_dirs = [                                               // 配置测试用例编译依赖包含目录
    "//third_party/googletest/googletest/include",
    "//third_party/bounds_checking_function/include",
    ......
  ]
  // 参与编译的源文件、配置和依赖
  sources = [ "//drivers/framework/ability/config/test/unittest/common/hdf_config_test.cpp" ] 
  deps = [
    "//base/hiviewdfx/hilog_lite/frameworks/featured:hilog_shared",
    ......
    "//drivers/adapter/uhdf/test/unittest/common:hdf_test_common",
  ]
  public_deps = [ "//third_party/bounds_checking_function:libsec_shared" ]
  cflags = [
    "-Wall",
   ......
  ]
}
```

## 4、资源配置文件介绍

​        配置资源文件路径为drivers\adapter\uhdf2\test\resource路径下的各模块目录下。target_name：测试单元的名字，通常定义在测试目录的BUILD.gn中；preparer：表示该测试单元执行前执行的动作；src=”res” ：表示编译输出后测试资源位于子系统根目录的resource目录；src=“out“ ：表示编译输出后测试资源位于out/release/$(子系统名)目录。具体实现以platform为例进行说明：

```
<configuration ver="2.0">
    <target name="hdf_adapter_uhdf_test_platform"> // 测试单元的名字
        <preparer>
            <option name="push" value="hdf/hdf/libhdf_test_common.z.so -> /system/lib" src="out"/> // 编译输出后测试资源位于out/release/$(子系统名)目录
        </preparer>
    </target>
</configuration>
```

​       在每个模块的用例的test/unittest目录下的BUILD.gn中添加依赖测试框架用户态库libhdf_test_common.z.so，具体实现如下示例所示。

```
 sources = [
    "//drivers/framework/support/platform/test/unittest/common/hdf_emmc_test.cpp",
   ......
  ]
 deps = [
    ......
    "//drivers/adapter/uhdf2/test/unittest/common:libhdf_test_common",
    ......
  ]  
```

***\*注意：\****依赖测试框架用户态库libhdf_test_common.z.so的编译输出路径为out\ohos-arm-release\tests\unittest\hdf中的对应模块下resource\hdf\hdf目录下。

​        如果手动测试时，需要将此文件push到设备system/lib目录下，如果用xdevice自测试工具，只需要配置资源xml文件，具体实现参考[测试子系统](https://gitee.com/openharmony/docs/blob/master/zh-cn/readme/测试子系统.md)中的使用测试框架及测试结果与日志章节。

## 5、测试

​      手动测试，具体测试步骤可参考[HDF测试指导](https://gitee.com/openharmony/vendor_huawei_hdf/blob/25bedb5d583f257a424fc113c1bcd1c6757e13b8/hdf_patch/HDF测试指导.md)。

