# Open Harmony HDF驱动自测试开发指导系列--系列一用例测试指导篇

- 简介
- 预置条件
- 用例测试



## 简介

​		HDF（Hardware Driver Foundation)自测试用例，用于测试HDF框架和外设的基本功能，提供用户态用例和内核态用例测试，本文主要介绍HDF内核态用例测试方法。

## 预置条件

1. 按照Open Harmony标准系统环境配置，[快速入门](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard.md)，完成环境配置和代码下载，并打上HDF patch。

2. 测试前需要代码全量编译通过。

3. 测试环境搭建，请参考  [测试平台使用指导](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-testguide-test.md#section76401945124810)。

4. 硬件环境以开源板HI3516DV300为例介绍。

   

## 用例测试方法

通过[HDC工具](https://gitee.com/openharmony/developtools_hdc_standard)把用例执行文件推送到设备中，然后执行用例即可，操作步骤如下

- 步骤1 全量编译，并烧录版本

- 步骤2 编译hdf测试用例

- 步骤3 用HDC工具推送测试文件到设备中

- 步骤4 进入设备data/test目录，执行测试文件即可

  

**全量编译，并烧录版本**

编译命令如下：

```
./build.sh --product-name Hi3516DV300
```

**编译hdf测试用例**

编译hdf测试用例命令和文件路径如下：

```
./build.sh --product-name Hi3516DV300 --build-target hdf_test

等待编译完成，生成的用例执行文件路径：
\out\ohos-arm-release\tests\unittest\hdf
此目录下的用例文件目录有devmgr、osal、sbuf、config
取用例文件推送到设备的data/test目录
```

**HDC工具推送文件到Hi3516DV300设备**

1.先在Hi3516DV300设备里新建data/test目录

```
mkdir -p data/test
```

2.推送依赖库和测试用吐送到到Hi3516DV300设备下

```
hdc file send XXX\out\ohos-arm-release\hdf\hdf\libhdf_test_common.z.so  /system/lib

hdc file send XXX\out\ohos-arm-release\tests\unittest\hdf\config\hdf_adapter_uhdf_test_config  /data/test
hdc file send XXX\out\ohos-arm-release\tests\unittest\hdf\devmgr\DevMgrTest  /data/test
hdc file send XXX\out\ohos-arm-release\tests\unittest\hdf\osal\OsalTest  /data/test
hdc file send XXX\out\ohos-arm-release\tests\unittest\hdf\sbuf\SbufTest  /data/test

```

**进入设备data/test目录，执行测试文件即可**

进入目录执行测试文件目录data/test

```
 cd data/test 
```

修改文件执行权限

```
chmod 777 hdf_adapter_uhdf_test_config DevMgrTest OsalTest SbufTest

./hdf_adapter_uhdf_test_config
./DevMgrTest
./OsalTest
./SbufTest
```

DevMgrTest用例结果显示：

```
./DevMgrTest                                                                                                                      
Running main() from gmock_main.cc                                                                                                   
[==========] Running 1 test from 1 test case.                                                                                       
[----------] Global test environment set-up.                                                                                        
[----------] 1 test from DevMgrTest                                                                                                 
[ RUN      ] DevMgrTest.DriverLoaderTest_001                                                                                        
[       OK ] DevMgrTest.DriverLoaderTest_001 (0 ms)                                                                                 
[----------] 1 test from DevMgrTest (0 ms total)                                                                                                                                                                                                             
[----------] Global test environment tear-down                                                                                      
Gtest xml output finished                                                                                                           
[==========] 1 test from 1 test case ran. (0 ms total)                                                                              
[  PASSED  ] 1 test.  
```

​     







