请检查提交PR是否满足[《HDI接口上库评审要求》](https://gitee.com/openharmony/drivers_interface/wikis/docs/DriversSIG_HDI_review_standard), 否则将不会审查通过。

|自查项|示例|
|:---|:---|
|是否通过HDI接口新增/变更评审？|**是**:申请PR review审查<br>**否**:申请SIG例会进行HDI接口评审[(点这里)](https://gitee.com/link?target=https%3A%2F%2Fshimo.im%2Fsheets%2F36GKhpvrXd8TcQHY%2FAdiEd)，未通过HDI接口评审的PR，不会审查通过|
|是否触发门禁构建？|评论命令"**start build**"触发门禁构建通过|
|是否门禁静态检查pass？|触发门禁构建大约60分钟后，查看构建结果和构建门禁静态检查结果，是否全部**pass**，未**pass**需要解决问题后重新触发构建|
|是否进行API规范检查？|参考[社区API设计规范](https://gitee.com/openharmony/docs/blob/master/zh-cn/design/OpenHarmony-API-quality.md)|
|是否进行OpenHarmony设备接口设计规范检查？|参考[OpenHarmony设备接口设计规范](https://gitee.com/openharmony/docs/blob/master/zh-cn/design/hdi-design-specifications.md)|
