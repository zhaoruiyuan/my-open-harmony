
import tarfile
import gzip
import os
import sys

# 解压第一层 gz
# def un_gz(file_name):
#     """ungz zip file"""
#     f_name = file_name.replace(".gz", "")
#     # 获取文件的名称，去掉
#     g_file = gzip.GzipFile(file_name)
#     # 创建gzip对象
#     open(f_name, "wb+").write(g_file.read())
#     # gzip对象用read()打开后，写入open()建立的文件里。
#     g_file.close()  # 关闭gzip对象

# 解压第二层tar
# def un_tar(file_name):
#     # untar zip file
#     tar = tarfile.open(file_name)
#     names = tar.getnames()
#     if os.path.isdir(file_name + "_files"):
#         pass
#     else:
#         os.mkdir(file_name + "_files")
#     # 由于解压后是许多文件，预先建立同名文件夹
#     for name in names:
#         tar.extract(name, file_name + "_files/")
#     tar.close()

def sendCmd(mycmd):
    result = "".join(os.popen(mycmd).readlines())
    return result

def getLatestFolder(path):
    # Func: 获取指定路径下的最新目录

    dirs = os.listdir(path)
    print("dirs = ", dirs)
    dirs.sort()
    return dirs[-1]

def tar_gz(local_dir,tar_exe):
    # local_dir  = sys.argv[1]
    # tar_exe = sys.argv[2]

    version_dir = getLatestFolder(local_dir)
    #local_dir 为sys.argv[1]中最新的文件
    local_dir = local_dir + "\\" + version_dir
    print("==================================local_dir Path================================")
    print(local_dir)

    # 解压第一层
    dirs = os.listdir(local_dir)
    dirs.sort()
    print("dirs = ", dirs)

    du200_test_tar_gz = local_dir + "\\" + dirs[-1]
    print("==================================du200_test_tar_gz Path================================")
    print(du200_test_tar_gz)
    # un_gz(du200_test_tar_gz)
    cmd_1 = tar_exe + " x " + du200_test_tar_gz + \
                     " -o\jenkins_worksp\RK3568\RK3568_burn\RK3568_version" + "\\" + version_dir
    print(cmd_1)
    sendCmd(cmd_1)

    du200_tar_gz = local_dir + "\\" + dirs[-2]
    print("==================================du200_tar_gz Path================================")
    print(du200_tar_gz)
    cmd_2 = tar_exe + " x " + du200_tar_gz + \
                     " -o\jenkins_worksp\RK3568\RK3568_burn\RK3568_version" + "\\" + version_dir
    print(cmd_2)
    sendCmd(cmd_2)

    dirs = os.listdir(local_dir)
    dirs.sort()
    print("dirs = ", dirs)

    # 解压第二层，使用本地解压工具解压。原因python解压不完全
    du200_test_tar = local_dir + "\\" + dirs[-2]
    print("==================================du200_test_tar Path================================")
    print(du200_test_tar)
    # un_gz(du200_test_tar)

    du200_tar = local_dir + "\\" + dirs[-4]
    print("==================================du200_tar Path================================")
    print(du200_tar)
    cmd_3 = tar_exe + " x " + du200_tar + \
            " -o\jenkins_worksp\RK3568\RK3568_burn\RK3568_version" + "\\" + version_dir + "\\" + "du200"
    print(cmd_3)
    sendCmd(cmd_3)

    cmd_4 = tar_exe + " x " + du200_test_tar + \
                     " -o\jenkins_worksp\RK3568\RK3568_burn\RK3568_version" + "\\" + version_dir + "\\" + "du200_test"
    print(cmd_4)
    sendCmd(cmd_4)
    dirs.sort()
    print("dirs = ", dirs)

    du200 = local_dir + "\\" + "du200"
    du200_test = local_dir + "\\" + "du200_test"
    print(du200)
    print(du200_test)
    sys.exit()