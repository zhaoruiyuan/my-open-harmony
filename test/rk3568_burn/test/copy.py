import os
import sys

import time
import shutil

def delFolder(path):
    # path: 路径
    # Func: 删除指定目录

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except PermissionError:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        except OSError:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        except BaseException:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        finally:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)

def createFolder(path):
    # path: 路径
    # Func: 创建指定目录

    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except PermissionError:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    except OSError:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    except BaseException:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    finally:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)

def xcopyCmd(src_dir, dst_dir):
    result_dir_name = "not null"
    if result_dir_name != "":
        src_path = src_dir
        dst_path = dst_dir
        print("Info: %s has not exist." % src_path)
        print("Info: %s has not exist." % dst_path)
        if os.path.exists(src_path):
            if os.path.exists(dst_path):
                print("dst_path file exist!")
                command = "xcopy %s %s /s /e /y /b /q" % (src_path, dst_path)
                print(command)
                if os.system(command) == 0:
                    print("Info: copy result successed.")
                    return True
                else:
                    print("Error: copy result failed.")
            else:
                print("Info: %s has not exist." % dst_path)
                return True
        print("src_path file not exist!")
    else:
        print("Error: result_dir_name is empty.")
    return False


def getLatestFolder(path):
    dirs = os.listdir(path)
    print("dirs= ", dirs)
    dirs.sort()
    return dirs[-1]

if __name__ == "__main__":
    # 源路径
    src_dir = sys.argv[1]
    # 目标路径
    dest_dir = sys.argv[2]
    reports_local_dir = sys.argv[3]
    #copy tests
    src_dir = sys.argv[1]
    print("======================================test server Path=====================================")
    print(src_dir)
    print("======================================test server Path=====================================")
    print("======================================test local Path=====================================")
    print(dest_dir)
    print("======================================test local Path=====================================")
    delFolder(dest_dir)
    createFolder(dest_dir)
    xcopyCmd(src_dir, dest_dir)
    print("==========================================================================================")
    print("======================================remove reports======================================")
    print("==========================================================================================")
    delFolder(reports_local_dir)
    createFolder(reports_local_dir)