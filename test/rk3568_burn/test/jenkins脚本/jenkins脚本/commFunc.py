import os
import time
import shutil



def delFolder(path):
    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except PermissionError:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        except OSError:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        except BaseException:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        finally:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)


def createFolder(path):
    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except PermissionError:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    except OSError:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    except BaseException:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    finally:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)


def getComponentsAndSuiteKV(path):
    suite_dict = {}
    component_list = []
    files_list = []
    for dirpath, dirnames, filenames in os.walk(path):
        component_list = dirnames
        if component_list!=[]:
            break
    for dirpath, dirnames, filenames in os.walk(path):
        if "resource" not in dirpath:
            for special_file in filenames:
                files_list.append(os.path.join(dirpath, special_file))
    for c_item in component_list:
        suite_list = []
        for f_item in files_list:
            if c_item in f_item:
                print(f_item.split("\\")[-1])
                suite_list.append(f_item.split("\\")[-1])
        suite_dict[c_item] = suite_list
    return suite_dict


def reOrderBin(dict, subSys):
    ordLst = []
    if "-" in subSys:
        ordLst = subSys.split("-")
    else:
        ordLst.append(subSys)
    ordDict = OrderedDict()
    initDict = OrderedDict(dict)
    delKeyDict = OrderedDict(dict)
    print("##################order_dict###################")
    print("initDict len: %s" %len(initDict))
    print("##################order_dict###################")
    for item in ordLst:
       for k,v in initDict.items():
           if item == k:
               ordDict[k] = v

    for item in ordLst:
       for k,v in delKeyDict.items():
           if item == k:
               del initDict[item]

    for k,v in initDict.items():
        ordDict[k] = v


    print("##################order_dict###################")
    print("ordDict len: %s" %len(ordDict))
    print("##################order_dict###################")
    for k,v in ordDict.items():
        print("Components Name: %s" %k)
    return ordDict


def runCasesWithSuite(path, componentName, suite_list = [], test_type = "UT"):
    try:
        print(path)
        print('当前路径为' + os.getcwd())
        os.chdir(path)
        print('改变后' + os.getcwd())
        for item in suite_list:
            run_cmd = "run -t" + test_type + " -ss " + componentName + " -ts " + item + "\n"
            print("OpenHarmony: " + run_cmd)
            with os.popen("start.bat", "w") as finput:
                finput.write("1\n")
                finput.write(run_cmd)
                finput.write("quit\n")
                finput.write("exit(0)\n")
    except Exception as e:
        return "runCasesWithSuite happened exception!"