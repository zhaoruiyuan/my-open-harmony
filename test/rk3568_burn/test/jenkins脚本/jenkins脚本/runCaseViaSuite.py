import os
import sys
import time
import shutil
current_dir = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(current_dir)[0]
sys.path.append(rootPath)

def getComponentsAndSuiteKV(path):
    suite_dict = {}
    component_list = []
    files_list = []
    for dirpath, dirnames, filenames in os.walk(path):
        component_list = dirnames
        if component_list!=[]:
            break
    for dirpath, dirnames, filenames in os.walk(path):
        if "resource" not in dirpath:
            for special_file in filenames:
                files_list.append(os.path.join(dirpath, special_file))
    for c_item in component_list:
        suite_list = []
        for f_item in files_list:
            if c_item in f_item:
                print(f_item.split("\\")[-1])
                suite_list.append(f_item.split("\\")[-1])
        suite_dict[c_item] = suite_list
    return suite_dict

def runCasesWithSuite(path, componentName, suite_list = [], test_type = "UT"):
    try:
        print(path)
        print('当前路径为' + os.getcwd())
        os.chdir(path)
        print('改变后' + os.getcwd())
        for item in suite_list:
            run_cmd = "run -t" + test_type + " -ss " + componentName + " -ts " + item + "\n"
            print("OpenHarmony: " + run_cmd)
            with os.popen("start.bat", "w") as finput:
                finput.write("1\n")
                finput.write(run_cmd)
                finput.write("quit\n")
                finput.write("exit(0)\n")
    except Exception as e:
        return "runCasesWithSuite happened exception!"

if __name__ == "__main__":
    developtest_path = sys.argv[1]
    test_type = sys.argv[2]
    testPath = sys.argv[3]

    print("=======================================================================")
    print("======================== copy partsInfo ===============================")
    print("=======================================================================")
    reports_local_dir = developtest_path + r"\reports"
    commFunc.delFolder(reports_local_dir)
    commFunc.createFolder(reports_local_dir)

    print("=======================================================================")
    print("========================== run testSuite ==============================")
    print("=======================================================================")
    type_lst = []
    for item in  test_type.split("-"):
        print(item)
        type_lst.append(item)
    for item in type_lst:
        if item == "unittest":
            print("~~Start to test unittest~~")
            dict = getComponentsAndSuiteKV(path=testPath + "\\unittest")
            print(str(dict))
            for k, v in dict.items():
                runCasesWithSuite(developtest_path, k, v, "UT")
        if item == "moduletest":
            print("~~Start to test moduletest~~")
            dict = commFunc.getComponentsAndSuiteKV(path=testPath + "\\moduletest")
            #for k, v in commFunc.reOrderBin(dict, subSys).itmes():
                #commFunc.runCasesWithSuite(developtest_path, k, v, "MST")
        if item == "systemtest":
            print("~~Start to test systemtest~~")
            dict = getComponentsAndSuiteKV(path=testPath + "\\systemtest")
            #for k, v in commFunc.reOrderBin(dict, subSys).itmes():
                #commFunc.runCasesWithSuite(developtest_path, k, v, "ST")