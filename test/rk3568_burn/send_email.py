import os, sys
import time
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def sendCmd(mycmd):
    result = "".join(os.popen(mycmd).readlines())
    return result

def getLatestFolder(path):
    # Func: 获取指定路径下的最新目录

    dirs = os.listdir(path)
    print("dirs = ", dirs)
    dirs.sort()
    return dirs[-1]

msg_from = "1924340120@qq.com"  # 发送方地址
pwd = "pqhcnoobjkndeaab"  # 授权密码
to = "chenchong43@huawei.com"  # 接收方地址

print("\n************************Started*********************")

# reports 所在目录
reports_dir = sys.argv[1]
print(reports_dir)


dirs = os.listdir(reports_dir)
print("dirs = ", dirs)
dirs.sort()

# 获取到测试结果目录
result_dir = reports_dir + "\\" + dirs[-2] # 测试结果目录名dir[-2]
print(result_dir)

# 构建日期
test_date = time.strftime("%Y-%m-%d")
subject = test_date + " HDF流水线执行报告"  # 邮件主题

# 组合邮件 包括附件 html 文本内容 alternative
# msg = MIMEMultipart('alternative')
msg = MIMEMultipart('alternative')

# 附件
att = MIMEText(open(reports_dir + "\\" + dirs[-2] + "\\" + dirs[-2] + ".zip", 'rb')
               .read(), 'base64', 'utf-8')
att["Content-Type"] = 'application/octet-stream'
att["Content-Disposition"] = 'attachment; filename=' + test_date + ".zip"
msg.attach(att)

# 文本
plain_content = "+++++++++++++++++++++"
msg.attach(MIMEText(plain_content, 'plain', 'utf-8'))

# html
html_content = open(reports_dir + "\\" + dirs[-2] + '\summary_report.html','rb').read()
msg.attach(MIMEText(html_content,'html','utf-8'))

# 构造邮件
msg["Subject"] = subject # msg为邮件内容对象
msg["Form"] = msg_from
msg["To"] = to

# 发送邮件
try:
    ss = smtplib.SMTP_SSL("smtp.qq.com", 465)  # 465为网易邮箱的端口号  ss是邮件对象
    ss.login(msg_from, pwd)
    ss.sendmail(msg_from, to, msg.as_string())  # 发送邮件
    print("邮件发送成功!")
except Exception as e:
    print("邮件发送失败!错误信息:", e)

print("************************Finished*********************\n")