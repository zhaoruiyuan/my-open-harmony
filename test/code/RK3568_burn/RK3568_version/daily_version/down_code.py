import json
import os
import time

import easygui
import requests
import collections



def url_get(input_list):
    one_payloadData = {
        "pageNum": 1,
        "pageSize": 100,
        "startTime": "%s000000" % (time.strftime("%Y%m%d", time.localtime())),
        "endTime": "",
        "projectName": "openharmony",
        "branch": "master",
        "component": "",
        "deviceLevel": "",
        "hardwareBoard": "",
        "buildStatus": "",
        "buildFailReason": "",
        "testResult": ""
    }
    postUrl = "http://ci.openharmony.cn/api/ci-backend/ci-portal/v1/dailybuilds"
    get_count = requests.post(postUrl, data=json.dumps(one_payloadData))

    two_payloadData = {
        "pageNum": 1,
        "pageSize": int(get_count.json()["result"]["total"]) + 10,
        "startTime": "%s000000" % (time.strftime("%Y%m%d", time.localtime())),
        "endTime": "",
        "projectName": "openharmony",
        "branch": "master",
        "component": "",
        "deviceLevel": "",
        "hardwareBoard": "",
        "buildStatus": "",
        "buildFailReason": "",
        "testResult": ""
    }
    r = requests.post(postUrl, data=json.dumps(two_payloadData))
    parent_url = r'http://download.ci.openharmony.cn/'

    # temp_dict = {}
    temp_dict = collections.OrderedDict()

    for i in r.json()["result"]["dailyBuildVos"]:
        name = i.get("component", None)
        if name == None:
            pass
        else:
            if input_list[-1] == name:
                get_create_time = i.get("buildStartTime")
                create_time = get_create_time[-6:]
                create_date = get_create_time[:-6]+" "+create_time[:2] + ":" + create_time[2:4] + ":" + create_time[4:]
                request_url = parent_url + i.get("obsPath")
                temp_dict[create_date] = request_url
    print(temp_dict)
    print(list(temp_dict)[-1], "---->", temp_dict[list(temp_dict)[-1]])
    # chioces_res = easygui.choicebox(msg="选择对应的时间段", title="选择", choices=list(temp_dict.keys()))
    url = temp_dict[list(temp_dict)[-1]]
    # print(url)
    time_split_list = list(temp_dict)[-1].split(" ")
    current_path = os.path.join(os.getcwd(), "version_" + time_split_list[0])
    if not os.path.exists(current_path):
        os.mkdir(current_path)
    file_name = os.path.join(current_path, (input_list[-1]+time_split_list[-1].replace(":", "")+".tar.gz"))
    print(file_name)
    progressbar(url, file_name)


# 进度条模块
def progressbar(url, file_name):
    # if not os.path.exists(path):   # 看是否有该文件夹，没有则创建文件夹
    #      os.mkdir(path)
    #  下载开始时间
    start = time.time()
    response = requests.get(url, stream=True)
    #  初始化已下载大小
    size = 0
    #  每次下载的数据大小
    chunk_size = 1024
    content_size = int(response.headers['content-length'])  # 下载文件总大小
    try:
        #   判断是否响应成功
        if response.status_code == 200:
            #  开始下载，显示下载文件大小
            print('Start download,[File size]:{size:.2f} MB'.
                  format(size=content_size / chunk_size / 1024))
            # 设置图片name，注：必须加上扩展名
            # 显示进度条
            with open(file_name, 'wb') as file:
                for data in response.iter_content(chunk_size=chunk_size):
                    file.write(data)
                    size += len(data)
                    # print(size*50)
                    # print(content_size * 100)
                    # print(size)

                    print('\r'+'[下载进度]:%s%.2f%%'
                          % ('>'*int(size*50 / content_size),
                             float(size / content_size * 100)),
                             end=' ')
        end = time.time()   # 下载结束时间
        print('Download completed!,times: %.2f秒' % (end - start))  #输出下载用时时间
    except:
        print('Error!')


if __name__ == '__main__':
    with open("config.txt", "r")as f:
        data = f.readlines()
    print(data)
    for i in data:
        temp_list = []
        temp_list.append(i.strip())
        url_get(input_list=temp_list)