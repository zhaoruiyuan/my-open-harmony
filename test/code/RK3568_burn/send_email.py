import os, sys
import time
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def sendCmd(mycmd):
    result = "".join(os.popen(mycmd).readlines())
    return result

def getLatestFolder(path):
    # Func: 获取指定路径下的最新目录

    dirs = os.listdir(path)
    print("dirs = ", dirs)
    dirs.sort()
    return dirs[-1]

print("\n************************Started*********************")

email_from = sys.argv[2]
msg_from = "1924340120@qq.com"  # 发送方地址
pwd = "pqhcnoobjkndeaab"  # 授权密码
# msg_to = ['chenchong43@huawei.com,guowen24@huawei.com,liufeihu@huawei.com']
# msg_to = ['353740058@qq.com']
msg_to = [email_from]
print("msg_to = ", msg_to)

to =','.join(msg_to) # 接收方地址
# to = "353740058@qq.com"  # 接收方地址

# 构建日期
test_date = time.strftime("%Y-%m-%d")
subject = test_date + " HDF流水线执行报告"  # 邮件主题

# 组合邮件 包括附件 html 文本内容 alternative
msg = MIMEMultipart('mixed')
# msg = MIMEMultipart('alternative')

# reports 所在目录
reports_dir = sys.argv[1]
print(reports_dir)

# reports目录下的所有文件
dirs = os.listdir(reports_dir)
print("dirs = ", dirs)
dirs.sort()

idx = 0
for file in dirs:
    print("file = ", file)
    if file == "latest":
        idx = idx + 1
    if idx == 0:
        # 获取到测试结果目录
        unittest_dir = reports_dir + "\\" + file
        print(unittest_dir)
        # 附件
        att_unittest = MIMEText(open(reports_dir + "\\" + file + "\\" + file + ".zip", 'rb')
                    .read(), 'base64', 'utf-8')
        att_unittest["Content-Type"] = 'application/octet-stream'
        att_unittest["Content-Disposition"] = 'attachment; filename=' + test_date + "-unittest.zip"
        msg.attach(att_unittest)

        # html
        html_unittest = open(reports_dir + "\\" + file + '\summary_report.html','rb').read()
        html_attr_unittest = MIMEText(html_unittest,'html','utf-8')
        html_attr_unittest["Content-Disposition"] = 'attachment; filename=' + test_date + "-uninttest.html"
        msg.attach(html_attr_unittest)
    if idx == 1:
        # 获取到测试结果目录
        fuzztest_dir = reports_dir + "\\" + file
        print(fuzztest_dir)
        # 附件
        att_fuzztest = MIMEText(open(reports_dir + "\\" + file + "\\" + file + ".zip", 'rb')
                                .read(), 'base64', 'utf-8')
        att_fuzztest["Content-Type"] = 'application/octet-stream'
        att_fuzztest["Content-Disposition"] = 'attachment; filename=' + test_date + "-fuzztest.zip"
        msg.attach(att_fuzztest)

        # html
        html_fuzztest = open(reports_dir + "\\" + file + '\summary_report.html','rb').read()
        html_attr_fuzztest = MIMEText(html_fuzztest,'html','utf-8')
        html_attr_fuzztest["Content-Disposition"] = 'attachment; filename=' + test_date + "-fuzztest.html"
        msg.attach(html_attr_fuzztest)

    idx = idx + 1

# 文本
plain_content = subject + "\nunittest测试结果:\n" + "    result: " + test_date + "-unittest.zip\n" + "    Test Summary: " + test_date + "-uninttest.html\n" +\
"\nfuzztest测试结果:\n" + "    result: " + test_date + "-fuzztest.zip\n" + "    Test Summary: " + test_date + "-fuzztest.html\n"
msg.attach(MIMEText(plain_content, 'plain', 'utf-8'))

# 构造邮件
msg["Subject"] = subject # msg为邮件内容对象
msg["Form"] = msg_from
msg["To"] = to

# 发送邮件
try:
    ss = smtplib.SMTP_SSL("smtp.qq.com", 465)  # 465为网易邮箱的端口号  ss是邮件对象
    ss.login(msg_from, pwd)
    ss.sendmail(msg_from, msg['To'].split(','), msg.as_string())  # 发送邮件
    print("邮件发送成功!")
except Exception as e:
    print("邮件发送失败!错误信息:", e)

print("************************Finished*********************\n")
