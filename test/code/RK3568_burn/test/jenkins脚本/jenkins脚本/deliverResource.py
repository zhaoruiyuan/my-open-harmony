import os
import sys
current_dir = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(current_dir)[0]
sys.path.append(rootPath)

import time
import shutil
from Common import xcopy
from Common import commFunc

if __name__ == "__main__":
    server_dir = sys.argv[1]
    img_local_dir = sys.argv[2]
    tests_local_dir = sys.argv[3]
    testPath = sys.argv[4]
    reports_local_dir = sys.argv[5]
    #copy_Image
    server_dir = server_dir + "\\" + xcopy.getLatestFolder(server_dir) + r"\Software\packages\phone\images"
    print("======================================Img server Path=====================================")
    print(server_dir)
    print("======================================Img server Path=====================================")
    print("======================================Img local Path=====================================")
    print(img_local_dir)
    print("======================================Img local Path=====================================")
    #创建目录并进行copy
    commFunc.delFolder(img_local_dir)
    commFunc.createFolder(img_local_dir)
    xcopy.xcopyCmd(server_dir, img_local_dir)
    if os.path.exists(testPath + "\\" + "version_path"):
        os.remove(testPath + "\\" + "version_path")
    commFunc.createFolder(testPath)
    file = open(testPath + "\\" + "version_path", "w")
    file.close()
    with open(testPath + "\\" + "version_path", "w") as fp:
        fp.write("VERSION_PATH=%s" %server_dir)
    #copy tests
    server_dir = sys.argv[1]
    server_dir = server_dir + "\\" + xcopy.getLatestFolder(server_dir) + r"\Software\packages\phone\images\tests"
    print("======================================test server Path=====================================")
    print(server_dir)
    print("======================================test server Path=====================================")
    print("======================================test local Path=====================================")
    print(tests_local_dir)
    print("======================================test local Path=====================================")
    commFunc.delFolder(tests_local_dir)
    commFunc.createFolder(tests_local_dir)
    xcopy.xcopyCmd(server_dir, tests_local_dir)
    print("==========================================================================================")
    print("======================================remove reports======================================")
    print("==========================================================================================")
    commFunc.delFolder(reports_local_dir)
    commFunc.createFolder(reports_local_dir)