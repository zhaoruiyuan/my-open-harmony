#
#
import sys
import time
import os
import re

def sendCmd(mycmd):
    result = "".join(os.popen(mycmd).readlines())
    return result

def send_times(mycmd):
    times = 0
    outcome = sendCmd(mycmd)
    while times < 3:
        if not outcome or outcome == "Empty":
            times += 1
            time.sleep(3)
        else:
            time.sleep(3)
            return outcome
    return outcome

def check_devices_mode():
    times = 0
    while times < 2:
        check_mode_cmd = "%s LD" % loadertool_path
        g = sendCmd(check_mode_cmd)
        print(g)
        if "Mode=Loader" in g:
            print("3568 board has entered the Loader mode successfully!")
            return True
        else:
            print("kill the process")
            os.system("hdc_std kill")
            time.sleep(5)
            print("start the process")
            os.system("hdc_std start")
            time.sleep(5)
            os.system("hdc_std shell reboot loader")
            time.sleep(10)
        times += 1
    print("Failed to enter the loader mode!")
    return False

def checkProcess():
    ren_cmd_1 = "hdc_std shell pidof render_service"
    u = sendCmd(ren_cmd_1)
    print("pid of render_service: %s" % u)
    if "[Fail]ExecuteCommand need connect-key" in u:
        print("device disconnection!")
        return False
    time.sleep(5)
    ren_cmd_2 = "hdc_std shell pidof render_service"
    v = sendCmd(ren_cmd_2)
    print("pid of render_service: %s" % v)
    time.sleep(5)
    laun_cmd_1 = "hdc_std shell pidof com.ohos.launcher"
    w = sendCmd(laun_cmd_1)
    print("pid of launcher: %s" % w)
    time.sleep(5)
    laun_cmd_2 = "hdc_std shell pidof com.ohos.launcher"
    x = sendCmd(laun_cmd_2)
    print("pid of launcher: %s" % x)
    if re.search('\d+', u) and re.search('\d+', w) and u == v and w == x:
        print("pids are same!")
        return True
    else:
        print("pids are not same!")
        return False

def flash_version():
    partList = ["boot_linux", "system", "vendor", "userdata", "resource"]
    for i in partList:
        loadcmd = "%s DI -%s %s/%s.img" % (loadertool_path, i, local_image_path, i)
        p = sendCmd(loadcmd)
        print(p)
        time.sleep(5)
        if "Download image ok" not in p:
            print("Failed to download the %s.img!" % i)
            return False
        else:
            print("The %s.img downloaded successfully!" % i)
    return True

def rk3568_upgrade(imgPath, toolPath):
    '''
    #======================================================
    #   @Method:        upgrade(self)
    #   @Precondition:  none
    #   @Func:          升级相关业务逻辑
    #   @PostStatus:    none
    #   @eg:            upgrade()
    #   @return:        True or Flase
    #=====================================================
    '''
    global local_image_path
    global loadertool_path
    local_image_path = imgPath
    loadertool_path = toolPath

    if not check_devices_mode():
        check_devices_cmd = "hdc_std list targets"
        f = send_times(check_devices_cmd)
        print(f)
        if not f or "Empty" in f:
            print("No devices found,please check the device.")
            return False
        else:
            print("3568 board is connected.")
            return check_devices_mode()
    else:
        upgrde_loader_cmd = "%s UL %s/MiniLoaderAll.bin -noreset" % (loadertool_path, local_image_path)
        h = sendCmd(upgrde_loader_cmd)
        print(h)
        if "Upgrade loader ok" not in h:
            print("Download MiniLoaderAll.bin Fail!")
            return False
        else:
            print("Download MiniLoaderAll.bin Success!")
            time.sleep(3)
            write_gpt_cmd = "%s DI -p %s/parameter.txt" % (loadertool_path, local_image_path)
            j = sendCmd(write_gpt_cmd)
            print(j)
            if "Write gpt ok" not in j:
                print("Failed to execute the parameter.txt")
                return False
            else:
                print("Successfully executed parameter.txt.")
                time.sleep(5)
                download_uboot_cmd = "%s DI -uboot %s/uboot.img %s/parameter.txt" % (
                    loadertool_path, local_image_path, local_image_path)
                k = sendCmd(download_uboot_cmd)
                print(k)
                if "Download image ok" not in k:
                    print("Failed to download the uboot.image!")
                    return False
                else:
                    print("The uboot.image downloaded successfully!")
                    time.sleep(5)
                    if not flash_version():
                        return False
                    reboot_devices_cmd = "%s RD" % loadertool_path
                    r = sendCmd(reboot_devices_cmd)
                    print(r)
                    time.sleep(40)
                    if "Reset Device OK" not in r:
                        print("Failed to reboot the board!")
                        return False
                    else:
                        print("Reboot successfully!")
                        print("******DownLoader img Finished and upload successfully******")
                        print("Start to check processes of the board!")
                        os.system("hdc_std kill")
                        time.sleep(5)
                        os.system("hdc_std start")
                        time.sleep(5)
                        times = 0
                        while times < 3:
                            if checkProcess():
                                time.sleep(5)
                                check_process_cmd = "hdc_std shell ps -elf"
                                t = sendCmd(check_process_cmd)
                                print(t)
                                list_ret = re.findall("render_service|com.ohos.launcher", t)
                                print(list_ret)
                                return True
                            else:
                                print("Try again!")
                                times += 1
                        print("Check the screen of the board!")
                        return False

if __name__ == "__main__":
    local_image_path = sys.argv[1]
    loadertool_path = sys.argv[2]
    rk3568_upgrade(local_image_path,loadertool_path)
    time.sleep(20)









