import os
import sys
import time
import shutil
current_dir = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(current_dir)[0]
sys.path.append(rootPath)

def delFolder(path):
    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except PermissionError:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        except OSError:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        except BaseException:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        finally:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)


def createFolder(path):
    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except PermissionError:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    except OSError:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    except BaseException:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    finally:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)

def getComponentsAndSuiteKV(path):
    suite_dict = {}
    component_list = []
    files_list = []
    for dirpath, dirnames, filenames in os.walk(path):
        component_list = dirnames
        if component_list!=[]:
            break
    for dirpath, dirnames, filenames in os.walk(path):
        if "resource" not in dirpath:
            for special_file in filenames:
                files_list.append(os.path.join(dirpath, special_file))
    for c_item in component_list:
        suite_list = []
        for f_item in files_list:
            if c_item in f_item:
                print(f_item.split("\\")[-1])
                suite_list.append(f_item.split("\\")[-1])
        suite_dict[c_item] = suite_list
    return suite_dict

# def runCasesWithSuite(path, componentName, suite_list = [], test_type = "UT"):
#     try:
#         print(path)
#         print('当前路径为' + os.getcwd())
#         os.chdir(path)
#         print('改变后' + os.getcwd())
#         for item in suite_list:
#             run_cmd = "run -t" + test_type + " -ss " + componentName + " -ts " + item + "\n"
#             print("OpenHarmony: " + run_cmd)
#             with os.popen("start.bat", "w") as finput:
#                 finput.write("1\n")
#                 finput.write(run_cmd)
#                 finput.write("quit\n")
#                 finput.write("exit(0)\n")
#     except Exception as e:
#         return "runCasesWithSuite happened exception!"

def runCasesWithSuite(path, componentName, test_type, test_module):
    try:
        print(path)
        print('当前路径为' + os.getcwd())
        os.chdir(path)
        print('改变后' + os.getcwd())

        if test_module == "all":
            run_cmd = "run -t " + test_type + " -ss " + componentName + "\n"
        else:
            run_cmd = "run -t " + test_type + " -ss " + componentName + " -tm " + test_module + "\n"
            
        print("OpenHarmony: " + run_cmd)
        with os.popen("start.bat", "w") as finput:
            finput.write("1\n")
            finput.write(run_cmd)
            finput.write("quit\n")
            finput.write("exit(0)\n")
    except Exception as e:
        return "runCasesWithSuite happened exception!"

if __name__ == "__main__":
    developtest_path = sys.argv[1]
    testPath = sys.argv[2]
    test_type = sys.argv[3]
    test_module = sys.argv[4]

    if test_type == "-all":
        test_type = "-ut-fuzz"
    print("test_type = ", test_type)

    
    print("=======================================================================")
    print("======================== copy partsInfo ===============================")
    print("=======================================================================")
    reports_local_dir = developtest_path + r"\reports"
    delFolder(reports_local_dir)
    createFolder(reports_local_dir)

    print("=======================================================================")
    print("========================== run testSuite ==============================")
    print("=======================================================================")
    type_lst = []
    for item in  test_type.split("-"):
        print(item)
        type_lst.append(item)
    for item in type_lst:
        if item == "ut":
            print("~~Start to test unittest~~")
            dict = getComponentsAndSuiteKV(path=testPath + "\\unittest")
            print(str(dict))
            runCasesWithSuite(developtest_path, "hdf", "UT", test_module)
            # for k, v in dict.items():
            #     runCasesWithSuite(developtest_path, "hdf", v, "UT")
        if item == "moduletest":
            print("~~Start to test moduletest~~")
            dict = commFunc.getComponentsAndSuiteKV(path=testPath + "\\moduletest")
            runCasesWithSuite(developtest_path, "hdf", "MST", test_module)
            #for k, v in commFunc.reOrderBin(dict, subSys).itmes():
                #runCasesWithSuite(developtest_path, k, v, "MST")
        if item == "systemtest":
            print("~~Start to test systemtest~~")
            dict = getComponentsAndSuiteKV(path=testPath + "\\systemtest")
            runCasesWithSuite(developtest_path, "hdf", "ST", test_module)
            #for k, v in commFunc.reOrderBin(dict, subSys).itmes():
                #runCasesWithSuite(developtest_path, k, v, "ST")
        if item == "fuzz":
            print("~~Start to test fuzztest~~")
            dict = getComponentsAndSuiteKV(path=testPath + "\\fuzztest")
            runCasesWithSuite(developtest_path, "hdf", "FUZZ", test_module)