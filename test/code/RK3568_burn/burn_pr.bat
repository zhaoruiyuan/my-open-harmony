
rem %1% -- version     %2% ---- email     %3% ---- testCaseType     %4% ---- moudle
set version=%1
set email=%2
set testCaseType=%3
set moudle=%4

cd D:\jenkins_worksp\RK3568\RK3568_burn\RK3568_version\web_pr_vesion

%cd%\download_web_pr_images.py %1

cd D:\jenkins_worksp\RK3568\RK3568_burn

%CD%\brun_tar.py %CD%\RK3568_version\web_pr_vesion D:\tools\FeiRar\4.2.1.120\FeiRarG.exe %CD%\RK3568_tool\RK3568_upgrade_tool.exe

%CD%\test\runCaseViaSuite.py D:\jenkins_worksp\RK3568\RK3568_burn\test\test_developertest D:\jenkins_worksp\RK3568\RK3568_burn\test\tests -%3 %4

cd D:\jenkins_worksp\RK3568\RK3568_burn

%CD%\send_email.py D:\jenkins_worksp\RK3568\RK3568_burn\test\test_developertest\reports %2

pause