setInterval(nowTime, 1000);

// 提交
$('.submitGo').click(function () {
    var _this = this;
    var versionAddress = $('.versionAddress').val();
    var mailAddress = $('.mailAddress').val();
    var mailArr = mailAddress.split(",");
    console.log(mailArr);
    var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    if (versionAddress) {
        $('text:eq(0)').text('');
        if (mailAddress) {
            for (var i = 0; i < mailArr.length; i++) {
                if (!myreg.test(mailArr[i])) {
                    $('text:eq(1)').text('提示\n\n请按照格式要求输入有效的E-mail！');
                    $('mailAddress').focus();
                    return false;
                }
            }

            // $.ajax({
            //     type : "POST",
            //     contentType: "application/json;charset=UTF-8",
            //     url : "http://127.0.0.1/admin/list/",
            //     data : JSON.stringify(list),
            //     success : function(result) {
            //         console.log(result);
            //     },
            //     error : function(e){
            //         console.log(e.status);
            //         console.log(e.responseText);
            //     }
            // });
            $('text').text('输入内容有效');

        }
    } else if (mailAddress && !versionAddress) {
        for (var i = 0; i < mailArr.length; i++) {
            if (!myreg.test(mailArr[i])) {
                $('text:eq(1)').text('提示\n\n请按照格式要求输入有效的E-mail！');
                $('mailAddress').focus();
                return false;
            }
        }
        $('text:eq(0)').text('输入框还未填写内容');
        $('text:eq(1)').text('输入内容有效');
        return false;
    } else {
        $('text').text('输入框还未填写内容');
        return false;
    }
    buttonTime(_this);
    $(this).addClass('touchStyle');
})

//回车触发点击事件
$("body").keypress(function (event) {
    if (event.which === 13) {
        if ($('.submitGo').hasClass('touchStyle') == false) {
            $('.submitGo').click();
        }
    }
})

// 操作不同的选项时， 触发样式改变
$('.to').focus(function () {
    $(this).addClass("inputStyle").siblings().removeClass("inputStyle");
}).blur(function () {
    $(this).removeClass("inputStyle");
})

// 防止按钮多次被点击提交
function buttonTime(e) {
    $(e).attr('disabled', 'true');
    setTimeout(function () {
        $(e).removeAttr('disabled');
        $('.submitGo').removeClass('touchStyle');
    }, 2000)
}

// 输出当前系统时间
function getNow(s) {
    return s < 10 ? '0' + s : s;
}

// 日期tip
function nowTime() {
    var tip = '';
    var myDate = new Date();
    var year = myDate.getFullYear();
    var month = myDate.getMonth() + 1;
    var date = myDate.getDate();
    var h = myDate.getHours();
    var m = myDate.getMinutes();
    var s = myDate.getSeconds();
    var ms = myDate.getMilliseconds();
    var weekDay = ["星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    var myDate = new Date(Date.parse("" + year + "/" + getNow(month) + "/" + getNow(date)));
    var now = year + '-' + getNow(month) + "-" + getNow(date) + " " + getNow(h) + ":" + getNow(m) + ":" + getNow(s) + ":" + getNow(ms);
    var nows = year + '-' + getNow(month) + "-" + getNow(date) + " " + getNow(h) + ":" + getNow(m) + ":" + getNow(s) + "&nbsp;&nbsp" + weekDay[myDate.getDay()];
    if (getNow(h) >= 0 && getNow(h) < 6) {
        tip = '🌙熬夜工作的同时也要注意身体';
    } else if (getNow(h) >= 6 && getNow(h) < 10) {
        tip = '🥛早上来杯牛奶会更有动力哦';
    } else if (getNow(h) >= 10 && getNow(h) < 14) {
        tip = '💤吃完午饭后午休一会儿更好哦';
    } else if (getNow(h) >= 14 && getNow(h) < 18) {
        tip = '☕下午可以喝咖啡提神哦';
    } else {
        tip = '🥣加班要记得吃晚饭哦';
    }
    $('.timeWeather span').html(nows + '<span class="tipText"> (' + tip + ') </span>');
    return now;
}