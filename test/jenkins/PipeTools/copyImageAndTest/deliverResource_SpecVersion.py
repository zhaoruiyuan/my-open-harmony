# -*- coding:utf-8 -*-
# Author:

import os
import sys
import time
import shutil

current_dir = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(current_dir)[0]
sys.path.append(rootPath)

from Common import xcopy
from Common import commFunc

if __name__ == "__main__":
    server_dir = sys.argv[1]
    tests_dir = sys.argv[2]
    img_local_dir = sys.argv[3]
    tests_local_dir = sys.argv[4]
    testPath = sys.argv[5]
    reports_local_dir = sys.argv[6]
    print("server_dir: %s" % server_dir)
    print("tests_dir: %s" % tests_dir)
    print("img_local_dir: %s" % img_local_dir)
    print("tests_local_dir: %s" % tests_local_dir)
    print("testPath: %s" % testPath)
    print("reports_local_dir: %s" % reports_local_dir)
    # copy images
    print("==================================Image Server Path================================")
    print(server_dir)
    print("==================================Image Server Path================================")
    print("==================================Image Local  Path================================")
    print(img_local_dir)
    print("==================================Image Local  Path================================")
    commFunc.delFolder(img_local_dir)
    commFunc.createFolder(img_local_dir)
    xcopy.xcopyCmd(server_dir, img_local_dir)
    if os.path.exists(testPath + "\\" + "version_path"):
        os.remove(testPath + "\\" + "version_path")
    commFunc.createFolder(testPath)
    file = open(testPath + "\\" + "version_path", "w")
    file.close()
    with open(testPath + "\\" + "version_path", "w") as fp:
        fp.write("VERSION_PATH = %s" %server_dir)
    # copy test
    print("===================================Test Server Path================================")
    print(tests_dir)
    print("===================================Tset Server Path================================")
    print("===================================Test Local  Path================================")
    print(tests_local_dir)
    print("===================================Test Local  Path================================")
    commFunc.delFolder(tests_local_dir)
    commFunc.createFolder(tests_local_dir)
    xcopy.xcopyCmd(tests_dir, tests_local_dir)
    print("===================================================================================")
    print("====================================Remove Reports=================================")
    print("===================================================================================")
    commFunc.delFolder(reports_local_dir)
    commFunc.createFolder(reports_local_dir)