# -*- coding:utf-8 -*-
# Author:

import os

def xcopyCmd(src_dir, dst_dir):
    # src_dir 源目录
    # dst_dir 目标目录
    # Func 拷贝测试结果到windows服务器

    result_dir_name = "not null"
    if result_dir_name != "":
        src_path = src_dir
        dst_path = dst_dir
        if os.path.exists(src_path):
            if os.path.exists(dst_path):
                command = "xcopy %s %s /s /e /y /b /q" % (src_path, dst_path)
                print(command)
                if os.system(command) == 0:
                    print("Info: copy result succeed.")
                    return True
                else:
                    print("Error: copy result failed.")
            else:
                print("Info: %s has exist." % dst_path)
                return True
    else:
        print("Error: result_dir_name is empty.")
    return False

def xcopyCmdFileCopy(src_dir, dst_dir):
    # Func: 拷贝测试结果到windows服务器

    result_dir_name = "not null"
    if result_dir_name != "":
        src_path = src_dir
        dst_path = dst_dir
        if os.path.exists(src_path):
            if os.path.exists(dst_path):
                command = "xcopy %s %s /c /f /y" % (src_path, dst_path)
                print(command)
                if os.system(command) == 0:
                    print("Info: copy result succeed.")
                    return True
                else:
                    print("Error: copy result failed.")
            else:
                print("Info: %s has exist." % dst_path)
                return True
    else:
        print("Error: result_dir_name is empty.")
    return False

def getFolderLst(path):
    # Func: 获取指定路径下的文件或目录

    folder_lst = []
    for dirpath, dirnames, filenames in os.walk(path):
        folder_lst = dirnames
        if folder_lst != []:
            break
    return folder_lst

def copyXMLtoTarget(source, target):
    # source: 源路径
    # target: 目标路径
    # Func: 拷贝源路径下资源至目标路径下

    result_lst = []
    for path in getFolderLst(source):
        if not path == "latest":
            result_lst.append(os.path.join(os.path.join(source, path), "result"))
    print(result_lst)

    sub_folderLst = []
    for item in result_lst:
        for dirpath, dirnames, filenames in os.walk(item):
            if dirnames != []:
                print(dirnames)
                if "systemtest" in dirnames:
                    sub_folderLst.append(os.path.join(dirpath, "systemtest"))
                    break
                if "moduletest" in dirnames:
                    sub_folderLst.append(os.path.join(dirpath, "moduletest"))
                    break
                if "unittest" in dirnames:
                    sub_folderLst.append(os.path.join(dirpath, "unittest"))
                    break
                xcopyCmd(item, target)
                break
    print(sub_folderLst)
    for item in sub_folderLst:
        xcopyCmd(item, target)

def getLatestFolder(path):
    # Func: 获取指定路径下的最新目录

    dirs = os.listdir(path)
    print("dirs = ", dirs)
    dirs.sort()
    return dirs[-1]