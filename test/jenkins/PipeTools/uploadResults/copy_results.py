#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2022. Huawei Technologies Co., Ltd. All rights reserved

# Func: 使用scp拷贝测试结果到日志服务器
# Return: 拷贝成功: True, 拷贝失败: False

import sys
import os
import subprocess

def get_latest_version_name(dir):
    # dir: 当前目录
    # Func: 获取最新的版本名称
    # Return: 返回最新的版本名称

    lists = []
    if os.path.exists(dir):
        name_list = os.listdir(dir)
        for item in name_list:
            if os.path.isdir(os.path.join(dir, item)):
                if item.startswith("2022"):
                    lists.append(item)
    lists.sort(key=lambda fn: os.path.getmtime(dir + os.sep + fn))
    return lists[-1]

def copy_results_to_windows(src_dir, dst_dir):
    # Func: 拷贝测试结果到windows服务器

    result_dir_name = get_latest_version_name(src_dir)
    if result_dir_name != "":
        src_path = os.path.join(src_dir, result_dir_name)
        dst_path = os.path.join(dst_dir, result_dir_name)

        print("------------------------------------------")
        print(src_dir)
        print(dst_dir)
        print(result_dir_name)
        print(src_path)
        print(dst_path)
        print("------------------------------------------")

        # 判断测试结果文件是否已经拷贝过
        if os.path.exists(src_path):
            if not os.path.exists(dst_path):
                os.makedirs(dst_path)
                command = "xcopy %s %s /s /e /y /b /q" % (src_path, dst_path)
                print(command)
                if os.system(command) == 0:
                    print("Info: copy result succeed.")
                    return True
                else:
                    print("Error: copy result failed.")
            else:
                print("Info: %s has exist." % dst_path)
                return True
    else:
        print("Error: result_dir_name is empty.")
    return False

def copy_results_to_linux(src_dir, dst_dir, pscpPath):
    # Func: 拷贝测试结果到Linux服务器

    result_dir_name = get_latest_version_name(src_dir)
    if result_dir_name != "":
        src_path = os.path.join(src_dir, result_dir_name)
        dst_path = "/home/huawei/workspace/" + dst_dir

        print("------------------------------------------")
        print(src_dir)
        print(dst_dir)
        print(result_dir_name)
        print(src_path)
        print(dst_path)
        print("------------------------------------------")

        # 判断测试结果文件是否已经拷贝过
        if os.path.exists(src_path):
            pscpPath = pscpPath + r"\Resource\exe\pscp.exe"
            pscp_tool_path = pscpPath
            host = "xx.xx.xx.xx"
            port = "22"
            username = "xxxxxx"
            password = "xxxxxx"

            command = "%s -P %s -pw %s -r %s %s@%s:%s" % (
                pscp_tool_path,
                port,
                password,
                src_path,
                username,
                host,
                dst_path)
            print(command)

            returncode = subprocess.call(command)
            if returncode != 0:
                print("Error: copy result files to linux failed.")
                return False
            else:
                print("Info: copy result files to linux succeed.")
                return True
    else:
        print("Error: result_dir_name is empty.")

    return False



if __name__ == '__main__':
    print("========================Started========================")
    src_dir = sys.argv[1]
    dst_dir = "report_list/" + sys.argv[2]
    pscpPath = sys.argv[3]
    copy_results_to_linux(src_dir, dst_dir, pscpPath)
    print("========================Finished========================")