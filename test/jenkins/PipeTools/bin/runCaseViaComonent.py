# -*- coding:utf-8 -*-
# Author:

import os
import sys
import time
import shutil

current_dir = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(current_dir)[0]
sys.path.append(rootPath)

from Common import commFunc
from Common import xcopy

if __name__ == "__main__":
    # developtest_path = r"D:\test\developertest"
    # 框架地址
    developtest_path = sys.argv[1]
    # 测试类型
    testType = sys.argv[2]
    # 用例路径
    testPath = sys.argv[3]
    # 工具路径
    toolPath = sys.argv[4]

    xcopy.xcopyCmdFileCopy(testPath + "\\" + "parts_info.json", toolPath + "\Resource\jenkins_res")
    reports_local_dir = developtest_path + r"\reports"
    commFunc.delFolder(reports_local_dir)
    commFunc.createFolder(reports_local_dir)

    with open(toolPath + "\\" + r"Resource\timestamp", "w") as fp:
        fp.write("START_TIME = %s" % time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))

    type_lst = []
    for item in testType.split("-"):
        type_lst.append(item)
    for item in type_lst:
        if item == "unittest":
            print("--Start to test unittest--")
            lst = commFunc.getComponentsName(path=testPath + "\\unittest")
            commFunc.runCaseWithComponents(developtest_path, lst, "UT")
        if item == "moduletest":
            print("--Start to test moduletest--")
            lst = commFunc.getComponentsName(path=testPath + "\\moduletest")
            commFunc.runCaseWithComponents(developtest_path, lst, "MST")
        if item == "systemtest":
            print("--Start to test systemtest--")
            lst = commFunc.getComponentsName(path=testPath + "\\systemtest")
            commFunc.runCaseWithComponents(developtest_path, lst, "ST")